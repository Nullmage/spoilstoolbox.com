"use strict";

const CONFIG = require("../config");

const Mail = require("../libs/mail");
const Log = require("../libs/logger").create("purge-inactive-accounts.log");
const Profiler = new (require("../libs/profiler"))("Start");
const Database = require("../config/rethinkdb");
const Accounts = require("../models/accounts");

Database.init(CONFIG.rethinkdb).then(() => {
	Profiler.tick("Database.init");
	return purge_inactive_accounts();
}).then(num_deleted => {
	Profiler.tick("purge_inactive_accounts", num_deleted);
	return Database.shutdown();
}).then(() => {
	Log(Profiler.tally("Database.shutdown"));
}).catch(err => {
	Mail.send_error({
		"subject": "Purge Inactive Accounts Failed",
		"message": Log(Profiler.tally("Error", err))
	});
});

function purge_inactive_accounts() {
	return new Promise((resolve, reject) => {
		Accounts.purge_inactive(
			(err, result) => err ? reject(err) : resolve(result.deleted)
		);
	});
}
