"use strict";

const CONFIG = require("../config");
const NUM_BACKUPS_TO_SHOW = 10;

const fs = require("fs");
const B2 = new (require("backblaze-b2"))(CONFIG.backblaze);

const Prompt = require("../libs/prompt").prompt;
const Database = require("../config/rethinkdb");

console.log("Authorizing with Backblaze...");
B2.authorize().then(result => {
	if (result.code) {
		throw result;
	}
	
	console.log("Gathering backups...");
	return B2.listFileNames({ "bucketId": CONFIG.backblaze.bucket });
}).then(result => {
	if (result.code) {
		throw result;
	}
	
	const backups = result.files.slice(-NUM_BACKUPS_TO_SHOW);
	const prompt_string = "\n------------------\nAvailable Backups:\n------------------\n\n" + enumerate_files(backups);
	const default_choice = backups.length - 1;
	
	return Prompt(`${prompt_string}\nChoose [default = ${default_choice}]: `).then(choice => {
		choice = Number(choice || default_choice);
		const target_backup = backups[choice];
		
		if (!target_backup) {
			throw new Error("Invalid backup selected");
		}
		
		console.log(`Downloading backup...${target_backup.fileName}`);
		return B2.downloadFileById(target_backup.fileId);
	}).then(result => {
		if (result.code) {
			throw result;
		}
		
		console.log("Writing backup to disc...");
		return write_dump(result.fileName, result.data);
	}).then(file_name => {
		console.log("Restoring database...");
		return Database.restore(file_name);
	}).then(file_name => {
		console.log("Removing backup...");
		return remove_dump(file_name);
	}).then(() => {
		console.log("...Done!");
	});
}).catch(err => {
	console.error(err);
});

function enumerate_files(files) {
	return files.reduce((result, entry, index) => {
		result += `${index}) ${entry.fileName}\n`;
		return result;
	}, "");
}

function write_dump(file_name, data) {
	return new Promise((resolve, reject) => {
		fs.writeFile(file_name, data, err => err ? reject(err) : resolve(file_name));
	});
}

function remove_dump(file_name) {
	return new Promise((resolve, reject) => {
		fs.unlink(file_name, err => err ? reject(err) : resolve());
	});
}