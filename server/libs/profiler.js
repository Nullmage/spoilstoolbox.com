"use strict";

const moment = require("moment");

const DECIMALS = 3;

class Profiler {
	constructor(name, data) {
		this.ticks = [];
		
		if (name) {
			this.tick(name, data);
		}
	}
	
	tick(name, data) {
		this.ticks.push({
			"time": moment().valueOf(),
			"name": name,
			"data": data
		});
	}
	
	tally(name, data) {
		this.tick(name || "End", data);
		
		const first_time = this.ticks[0].time;
		this.ticks[0].time = 0;
		
		for (let i = 1; i < this.ticks.length; ++i) {
			const entry = this.ticks[i];
			
			entry.time = (entry.time - first_time) / 1000;
			entry.time = round(entry.time, DECIMALS);
			entry.diff = entry.time - this.ticks[i - 1].time;
			entry.diff = round(entry.diff, DECIMALS);
		}
		
		return this.ticks;
	}
}

function round(n, decimals) {
	return Number(n.toFixed(decimals));
}

module.exports = Profiler;