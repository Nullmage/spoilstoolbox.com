"use strict";

exports.clone = function(obj) {
	return JSON.parse(JSON.stringify(obj));
};

exports.enumize = function(arr) {
	return arr.reduce(function(o, v) {
		o[v] = v;
		return o;
	}, {});
};
