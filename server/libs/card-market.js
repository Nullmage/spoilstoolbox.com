"use strict";

const request = require("request");
const crypto = require("crypto");

const BASE_URI = "https://www.mkmapi.eu/ws/v1.1/output.json/";
const OAUTH_SIGNATURE_METHOD = "HMAC-SHA1";
const OAUTH_VERSION = "1.0";
const TIMEOUT = 5000;

class CardMarket {
	constructor(options) {
		this.app_token = options.app_token;
		this.app_secret = options.app_secret;
	}

	products(name, callback) {
		const encoded_name = this._encode_name(name);
		const uri = BASE_URI + `products/${encoded_name}/5/1/true`;

		request({
			"uri": uri,
			"method": "GET",
			"timeout": TIMEOUT,
			"headers": {
				"Authorization": this._create_auth_header(uri)
			}
		}, function(err, res, body) {
			if (err) {
				callback(err);
			} else if (res.statusCode !== 200) {
				callback(body);
			} else {
				callback(null, JSON.parse(body).product);
			}
		});
	}

	_encode_name(name) {
		return encodeURIComponent(name).replace(/['!]/g, function(c) {
			switch (c) {
				case "'":	return "%27";
				case "!":	return "%21";
			}
		});
	}

	_create_auth_header(uri) {
		const nonce = crypto.randomBytes(8).toString("hex");
		const timestamp = Math.floor(Date.now() / 1000);
		const signature = this._create_signature(uri, nonce, timestamp);

		return `OAuth realm="${uri}", oauth_consumer_key="${this.app_token}", oauth_token="", oauth_nonce="${nonce}", oauth_timestamp="${timestamp}", oauth_signature_method="${OAUTH_SIGNATURE_METHOD}", oauth_version="${OAUTH_VERSION}", oauth_signature="${signature}"`;
	}

	_create_signature(uri, nonce, timestamp) {
		const encoded_params = `GET&${encodeURIComponent(uri)}&` +
			encodeURIComponent(`oauth_consumer_key=${this.app_token}&`) +
			encodeURIComponent(`oauth_nonce=${nonce}&`) +
			encodeURIComponent(`oauth_signature_method=${OAUTH_SIGNATURE_METHOD}&`) +
			encodeURIComponent(`oauth_timestamp=${timestamp}&`) +
			encodeURIComponent(`oauth_token=&`) +
			encodeURIComponent(`oauth_version=${OAUTH_VERSION}`);

		const signing_key = encodeURIComponent(this.app_secret) + "&";
		const signature = crypto.createHmac("sha1", signing_key).update(encoded_params).digest("base64");

		return signature;
	}
}

exports.CardMarket = CardMarket;
