"use strict";

const http_status = require('statuses');

class HttpError {
	constructor(status, err) {
		this.status = status;
		this.message = http_status[status];
		this.err = err;
	}
}

exports.http = {
	"unauthorized":	err => new HttpError(401, err),
	"forbidden":	err => new HttpError(403, err),
	"not_found":	err => new HttpError(404, err),
	"internal":		err => new HttpError(500, err)
};

// Convert an array of form errors from express-validate to
// errors we can handle on the front end. Only the first
// error for each field is included.
exports.form = function(errors) {
	const seen = {};

	return errors.reduce(function(result, error) {
		const field = error.param;

		if (seen[field] === undefined) {
			result.push(form_field(field, error.msg));
			seen[field] = true;
		}

		return result;
	}, []);
};

function form_field(field, code) {
	return {
		"field": field,
		"code": code
	};
}

exports.form_field = form_field;
