"use strict";

const tools = require("../libs/tools");

module.exports = {
	"toArray": function(value) {
		return Array.isArray(value) ? value : [value];
	},
	
	"toIntArray": function(value) {
		return value.map(x => parseInt(x, 10));
	},
	
	"enumize": function(value) {
		return Array.isArray(value) ? tools.enumize(value) : value;
	},
	
	"split": function(value, splitter) {
		return value.split(splitter);
	},
	
	"filter": function(value, filter) {
		if (Array.isArray(value)) {
			return value.filter(filter);
		} else if (filter(value)) {
			return value;
		}
	}
};
