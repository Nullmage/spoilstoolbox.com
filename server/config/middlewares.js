"use strict";

const path = require("path");

module.exports = function(express) {
	express.set("port", process.env.PORT || 3000);
	express.set("views", path.join(SERVER_ROOT, "views"));
	express.set("view engine", "ejs");

	const body_parser = require("body-parser");
	express.use(body_parser.urlencoded({
		"extended":	true,
		"inflate":	false // nginx will take care of this
	}));

	express.use(body_parser.json({
		"inflate":	false
	}));

	const express_validator = require("express-validator");
	express.use(express_validator({
		"customValidators": require("./validators"),
		"customSanitizers": require("./sanitizers")
	}));
	
	const client_sessions = require("client-sessions");
	express.use(client_sessions({
		"cookieName": "session",
		"secret": "be1438bcf37f84cad54933382ec3c7d4635e0ff3",
		"duration": 24 * 60 * 60 * 1000,
		"ephemeral": true,
		"cookie": {
			"secureProxy": ENVS.prod
		}
	}));

	if (ENVS.dev) {
		const morgan = require("morgan");
		express.use(morgan("dev"));
	}

	if (ENVS.dev || ENVS.test) {
		// e2e tests needs this
		const serve_static = require("express").static;
		express.use(serve_static(CLIENT_ROOT));
	}

	// Security
	express.disable("x-powered-by");
};
