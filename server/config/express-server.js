"use strict";

let server;

function init(options) {
	return new Promise(resolve => {
		const express = require("express")();
		
		require("./middlewares")(express);
		require("./routes")(express);

		server = require("http").createServer(express);
		server.listen(options.port, () => resolve(server));
	});
}

function shutdown() {
	return new Promise(resolve => server.close(resolve));
}

module.exports = {
	"init": init,
	"shutdown": shutdown
};
