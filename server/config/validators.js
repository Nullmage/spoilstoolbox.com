"use strict";

const is_disposable_email = require("is-disposable-email");

module.exports = {
	"isNotDisposableEmail": function(value) {
		if (value && value.indexOf("@") !== -1) {
			return !is_disposable_email(value);
		}

		// Invalid email addresses are not considered disposable
		return true;
	},

	"isAsyncPredicateTrue": function(value, async_predicate) {
		return new Promise((resolve, reject) => {
			async_predicate(value, function(err, result) {
				if (err) {
					reject(err);
				} else if (result) {
					resolve();
				} else {
					reject();
				}
			});
		});
	},

	"isAsyncPredicateFalse": function(value, async_predicate) {
		return new Promise((resolve, reject) => {
			async_predicate(value, function(err, result) {
				if (err) {
					reject(err);
				} else if (!result) {
					resolve();
				} else {
					reject();
				}
			});
		});
	}
};
