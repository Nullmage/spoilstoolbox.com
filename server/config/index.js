"use strict";

const path = require("path");

global.PROJECT_ROOT = path.normalize(path.join(__dirname, "..", ".."));
global.CLIENT_ROOT = path.join(PROJECT_ROOT, "client");
global.SERVER_ROOT = path.join(PROJECT_ROOT, "server");

global.ENV = process.env.NODE_ENV || "development";

global.ENVS = {
	"prod": ENV === "production",
	"dev":	ENV === "development",
	"test":	ENV === "test"
};

module.exports = {
	"express_server": {
		"port":				process.env.PORT || 3000
	},

	"elasticsearch": {
		"host":				"localhost:9200"
	},

	"rethinkdb": {
		"host":				"localhost",
		"port":				28015,
		"db":				ENVS.test ? "spoils_test" : "spoils",
		"silent":			ENVS.prod,
		"schema": 			require("./schema")
	},

	"mailgun": {
		"privateApi":		"key-948f81cebc339fbc8974dd8306ce73dc",
		"publicApi":		"pubkey-67d1828c9fb4b6a1859e42a11706b8a3",
		"domainName":		"spoilstoolbox.com"
	},
	
	"backblaze": {
		"accountId":		"ae680d1eb291",
		"applicationKey":	"00161925ed1ef272a5b5a646f49483a7434df23d57",
		"bucket":			"9adee618e02d312e5b420911"
	},

	"cardmarket": {
		"app_token":		"qkOoBSbGVlg3GgDS",
		"app_secret":		"vO3E2A2bviWxNLyJ25sqJ4YUXaPTV7po"
	}
};
