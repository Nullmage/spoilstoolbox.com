"use strict";

const error = require("../libs/error");

function protect(req, res, next) {
	if (req.session.authed) {
		const csrf_token = req.get("x-csrf-token");
		
		if (req.session.csrf_token === csrf_token) {
			next();
		} else {
			next(error.http.forbidden({
				"csrf-token": req.session.csrf_token,
				"csrf-token-header": csrf_token
			}));
		}
	} else {
		next(error.http.unauthorized());
	}
}

module.exports = function(app) {
	const index = require("../controllers/index");

	app.post("/contact", index.contact);
	app.post("/client-error", index.client_error);
	app.get("/sign-out", index.sign_out);

	const account = require("../controllers/account");

	app.post("/account", account.create);
	app.patch("/account/activate", account.activate);
	app.post("/account/authenticate", account.authenticate);
	app.post("/account/recover", account.recover);
	app.put("/account/password", account.set_password);
	app.get("/account/settings", protect, account.settings);
	app.patch("/account/password", protect, account.change_password);

	const card = require("../controllers/card");

	app.post("/cards/search", card.search);
	app.get("/cards/:name/info", card.info);
	app.get("/cards/:name/price", card.price);

	const deck = require("../controllers/deck");

	app.post("/decks/search", deck.search);
	app.get("/decks", deck.recent);
	app.get("/decks/recent/:page?", deck.recent);
	app.get("/decks/mine/:page?", protect, deck.mine);
	app.get("/decks/:id", deck.find_by_id);
	app.post("/decks", protect, deck.create);
	app.patch("/decks/:id", protect, deck.update);
	app.patch("/decks/:id/publish", protect, deck.publish);
	app.patch("/decks/:id/unpublish", protect, deck.unpublish);
	app.delete("/decks/:id", protect, deck.trash);

	const deck_builder = require("../controllers/deck-builder");

	app.get("/deck-builder/proxies", deck_builder.proxies);
	app.post("/deck-builder/parse_from_file", deck_builder.parse_from_file);
	
	// For cache busting
	const RND = Math.random().toString().substr(2);
	
	app.get("*", function(req, res) {
		res.render("index", {
			"ENVS": ENVS,
			"RND": RND
		});
	});

	require("../middlewares/error-handlers")(app);
};
