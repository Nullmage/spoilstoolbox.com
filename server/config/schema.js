"use strict";

module.exports = {
	"tables": [
		"accounts",
		"decks"
	],

	"indexes": {
		"accounts": [
			{ "name": "email" },
			{ "name": "username" },
			{ "name": "activation_token" },
			{ "name": "recovery_token" }
		],

		"decks": [
			{ "name": "username" },
			{ "name": "published" },
			{ "name": "published_at" },
			{ "name": "id_username", "props": ["id", "username"] },
			{ "name": "cards", "props": { "multi": true } }
		]
	}
};
