"use strict";

process.on("SIGINT", () => process.exit(1));

const CONFIG = require("./config");

global.CardMarket = new (require("./libs/card-market")).CardMarket(CONFIG.cardmarket);

const Elasticsearch = require("./config/elasticsearch");
const Database = require("./config/rethinkdb");
const ExpressServer = require("./config/express-server");

function init(callback) {
	Elasticsearch.init(CONFIG.elasticsearch)
	.then(() => Database.init(CONFIG.rethinkdb))
	.then(() => ExpressServer.init(CONFIG.express_server))
	.then(server => callback(null, server))
	.catch(callback);
}

function shutdown(callback) {
	ExpressServer.shutdown()
	.then(() => Database.shutdown())
	.then(() => Elasticsearch.shutdown())
	.then(callback).catch(callback);
}

if (require.main === module) {
	init(err => {
		if (err) {
			console.error(err.stack ? err.stack : err);
			process.exit(1);
		} else {
			console.log("Application running...");
		}
	});
}

module.exports = { "init": init, "shutdown": shutdown };
