"use strict";

const error = require("../libs/error");
const Mail = require("../libs/mail");
const Log = require("../libs/logger").create("client-errors.log");

exports.sign_out = function(req, res) {
	req.session.destroy();
	res.status(200).end();
};

exports.contact = function(req, res, next) {
	// If no email was provided, send a mail from
	// "feedback@spoilstoolbox.com" -> "martin.tberger@gmail.com"
	// If an email was provided, send a mail from
	// 'email' -> "feedback@spoilstoolbox.com" (which gets forwarded to "martin.tberger@gmail.com")
	// This way, we can easily reply to the user directly from Gmail.

	let from = req.body.email;
	let to = "martin.tberger@gmail.com";

	if (from) {
		to = "feedback@spoilstoolbox.com";
	} else {
		from = "feedback@spoilstoolbox.com";
	}
	
	Mail.send({
		"from": from, "to": to,
		"subject": req.body.subject,
		"message": req.body.message.trim()  
	}).then(() => {
		res.status(200).end();
	}).catch(err => next(error.internal(err)));
};

exports.client_error = function(req, res) {
	Log(req.body);
	res.status(200).end();
};
