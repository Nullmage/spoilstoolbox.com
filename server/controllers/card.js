"use strict";

const cache = require("memory-cache");

const Log = require("../libs/logger").create("card-queries.log");
const error = require("../libs/error");
const Cards = require("../models/cards");

function sanitize_search(req, res, next) {
	req.body.filters = req.body.filters || [];
	req.body.options = req.body.options || [];

	req.sanitizeBody("filters").toArray();
	req.sanitizeBody("options").toArray();
	req.sanitizeBody("options").enumize();
	
	if (ENVS.prod) {
		Log(req.body);
	}
	
	next();
}

function search(req, res, next) {
	Cards.find_by_query(req.body, (err, result) => {
		if (err) {
			if (err instanceof Error) {
				res.status(400).json(err.message || "Bad Request");
			} else {
				next(error.http.internal(err));
			}
		} else if (!result) {
			res.status(204).end();
		} else {
			res.status(200).json(result);
		}
	});
}

exports.search = [sanitize_search, search];

exports.info = function(req, res, next) {
	const OPTIONS = { "projection": "full" };
	const card_name = req.params.name;
	
	Cards.find_by_name(card_name, OPTIONS, (err, cards) => {
		if (err) {
			next(error.http.internal(err));
		} else if (!cards) {
			res.status(204).end();
		} else {
			const card = cards[0];

			res.status(200).json({
				"name": card_name,
				"text": card.text,
				"format": card.format
			});
		}
	});
};

exports.price = function(req, res, next) {
	const card_name = req.params.name;
	const prices = cache.get(card_name);

	if (prices) {
		return res.status(200).json(prices);
	}

	CardMarket.products(card_name, (err, prices) => {
		if (err && err.code === "ETIMEDOUT") {
			return res.status(408).end();
		} else if (err) {
			return next(error.http.internal(err));
		}

		// Sort so the latest sets are shown first
		prices = prices.sort((p1, p2) => p1.idProduct < p2.idProduct);

		// Filter out stuff we are interested in
		prices = prices.reduce((prices, product) => {
			prices.push({
				"set": product.expansion,
				"uri": "https://en.spoilscardmarket.eu" + product.website,
				"price": product.priceGuide.LOW
			});

			return prices;
		}, []);

		cache.put(card_name, prices, 24 * 60 * 60 * 1000);
		res.status(200).json(prices);
	});
};
