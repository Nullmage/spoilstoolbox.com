"use strict";

const Log = require("../libs/logger").create("deck-queries.log");
const error = require("../libs/error");
const Decks = require("../models/decks");
const Cards = require("../models/cards");

const PAGE_SIZE = 20;

function sanitize_search(req, res, next) {
	req.sanitizeBody("username").trim();
	req.sanitizeBody("trades").toArray();
	req.sanitizeBody("cards").split(",");
	req.sanitizeBody("cards").filter(Boolean);
	
	if (ENVS.prod) {
		Log(req.body);
	}
	
	next();
}

function search(req, res, next) {
	Decks.find_by_query(req.body, (err, decks) => {
		if (err) {
			next(error.http.internal(err));
		} else {
			res.status(200).json(decks);
		}
	});
}

exports.search = [sanitize_search, search];

function start_index(page) {
	return Math.max(page, 0) * PAGE_SIZE;
}

function end_index(page) {
	return (page + 1) * PAGE_SIZE;
}

exports.recent = function(req, res, next) {
	req.sanitizeParams("page").toInt();
	
	const page = req.params.page || 0;
	const start = start_index(page);
	const end = end_index(page);
	
	Decks.find_by_recency(start, end, (err, decks) => {
		if (err) {
			next(error.http.internal(err));
		} else {
			res.status(200).json(decks);
		}
	});
};

function validate_deck(req, res, next) {
	req.sanitizeBody("name").escape();
	req.checkBody("name", "DECK.NAME_REQUIRED").notEmpty();
	req.sanitizeBody("public").toBoolean();

	req.asyncValidationErrors().then(() => next()).catch(errors => {
		res.status(400).json(error.form(errors));
	});
}

function create(req, res, next) {
	const deck = {
		"username":		req.session.account.username,
		"name":			req.body.name,
		"cards":		req.body.cards,
		"threshold":	req.body.threshold,
		"published":	false
	};

	Decks.create(deck, err => {
		if (err) {
			next(error.http.internal(err));
		} else {
			res.status(201).end();
		}
	});
}

exports.create = [validate_deck, create];

exports.mine = function(req, res, next) {
	req.sanitizeParams("page").toInt();
	
	const username = req.session.account.username;
	const page = req.params.page || 0;
	const start = start_index(page);
	const end = end_index(page);

	Decks.find_by_username(username, start, end, (err, decks) => {
		if (err) {
			next(error.http.internal(err));
		} else {
			res.status(200).json(decks);
		}
	});
};

exports.find_by_id = function(req, res, next) {
	const deck_id = req.params.id;
	Decks.find_by_id(deck_id, (err, deck) => {
		if (err) {
			return next(error.http.internal(err));
		} else if (!deck.id) {
			return next(error.http.not_found(deck_id));
		}
		
		if (!deck.cards) {
			return res.status(200).json(deck);
		}

		Cards.find_by_ids(deck.cards, (err, cards) => {
			if (err) {
				return next(error.http.internal(err));
			}
			
			deck.cards = deck.cards.map(card_id => {
				return cards.find(card => card.id === card_id);
			});
			
			res.status(200).json(deck);
		});
	});
};

function update(req, res, next) {
	const deck = {
		"username":		req.session.account.username,
		"id":			req.params.id,
		"name":			req.body.name,
		"cards":		req.body.cards,
		"threshold":	req.body.threshold
	};

	Decks.update(deck, (err, updated) => {
		if (err) {
			next(error.http.internal(err));
		} else if (!updated) {
			next(error.http.forbidden(deck));
		} else {
			res.status(200).end();
		}
	});
}

exports.update = [validate_deck, update];

exports.publish = function(req, res, next) {
	const id = req.params.id;
	const username = req.session.account.username;
	Decks.publish(id, username, (err, success) => {
		if (err) {
			next(error.http.internal(err));
		} else if (!success) {
			next(error.http.forbidden({ "id": id, "username": username }));
		} else {
			res.status(200).end();
		}
	});
};

exports.unpublish = function(req, res, next) {
	const id = req.params.id;
	const username = req.session.account.username;
	Decks.unpublish(id, username, (err, success) => {
		if (err) {
			next(error.http.internal(err));
		} else if (!success) {
			next(error.http.forbidden({ "id": id, "username": username }));
		} else {
			res.status(200).end();
		}
	});
};

exports.trash = function(req, res, next) {
	const id = req.params.id;
	const username = req.session.account.username;
	Decks.trash(id, username, (err, success) => {
		if (err) {
			next(error.http.internal(err));
		} else if (!success) {
			next(error.http.forbidden({ "id": id, "username": username }));
		} else {
			res.status(200).end();
		}
	});
};
