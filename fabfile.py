execfile("../fabric/fabfile.py")

import credentials

env.roledefs = credentials.ROLEDEFS
env.passwords = credentials.PASSWORDS

APP_DIR = "spoilstoolbox.com"
REPOSITORY = "git@bitbucket.org:GlurG/spoilstoolbox.com.git"

CERTIFICATE_EMAIL = "cert@spoilstoolbox.com"
CERTIFICATE_DOMAINS = ["spoilstoolbox.com", "www.spoilstoolbox.com"]

@task
def setup_application_server():
	install_elasticsearch()
	config_elasticsearch()
	install_rethinkdb()
	install_nodejs()
	install_certbot()
	obtain_certificates(CERTIFICATE_EMAIL, CERTIFICATE_DOMAINS)
	install_nginx()
	config_nginx()
	install_git()
	install_pm2()
	clone_repository()
	install_npm_packages()
	build_client()
	update_remote_database()
	update_crontab()
	boot_application()
	reboot()

@task
def config_elasticsearch():
	sudo("mkdir -p /etc/elasticsearch/scripts")
	put(config_file("sort_by_threshold.groovy"), "/etc/elasticsearch/scripts/sort_by_threshold.groovy", use_sudo = True)

@task
def config_nginx():
	files.upload_template(config_file("spoilstoolbox.com"), "/etc/nginx/sites-available/spoilstoolbox.com", {
		"root_dir": "/home/%s/%s/client" % (env.user, APP_DIR)
	}, use_sudo = True)

	if not cuisine.file_exists("/etc/nginx/sites-enabled/spoilstoolbox.com"):
		sudo("ln -s /etc/nginx/sites-available/spoilstoolbox.com /etc/nginx/sites-enabled/spoilstoolbox.com")

	sudo("service nginx restart")

def install_pm2():
	run("npm install -g pm2")
	sudo("env PATH=$PATH:$HOME/npm/bin pm2 startup ubuntu -u %s" % env.user)
	sudo("chown -R %s .pm2/" % env.user)

def build_client():
	with cd(APP_DIR):
		sudo("npm run build:client")

def clone_repository():
	if cuisine.dir_exists(APP_DIR):
		return

	echo_ssh_public_key()
	prompt("Make sure you have added this machine's public SSH key to Github/Bitbucket before proceeding...")
	run("git clone %s %s" % (REPOSITORY, APP_DIR))

@task
def install_npm_packages():
	with cd(APP_DIR):
		sudo("npm install --production")

@task
def update_remote_database():
	prompt("Before updating remote ElasticSearch, make sure to run export2es.py to generate fresh cards.json")
	
	with lcd("content/tools/elasticsearch"):
		sudo("curl -X DELETE http://localhost:9200/spoils")

		put("settings.json", "settings.json", use_sudo = True)
		sudo("curl -X PUT http://localhost:9200/spoils -d @settings.json")
		sudo("rm settings.json")

		sudo("curl -X DELETE http://localhost:9200/spoils/card")

		put("card-mapping.json", "card-mapping.json", use_sudo = True)
		sudo("curl -X PUT http://localhost:9200/spoils/_mapping/card -d @card-mapping.json")
		sudo("rm card-mapping.json")
	
	with lcd("content/tools"):		
		put("cards.json", "cards.json", use_sudo = True)
		sudo("curl -X POST http://localhost:9200/spoils/card/_bulk --data-binary @cards.json")
		sudo("rm cards.json")

	sudo("curl -X POST http://localhost:9200/spoils/_refresh")

@task
def update_crontab():
	put(config_file("cronjobs"), "cronjobs")
	run("crontab cronjobs")
	run("rm cronjobs")

@task
def update_database():
	with lcd("content/elasticsearch"):
		local("curl -X DELETE http://localhost:9200/spoils")
		local("curl -X PUT http://localhost:9200/spoils -d @settings.json")
		local("curl -X DELETE http://localhost:9200/spoils/card")
		local("curl -X PUT http://localhost:9200/spoils/_mapping/card -d @card-mapping.json")

	with lcd("content/cards"):
		local("curl -X POST http://localhost:9200/spoils/card/_bulk --data-binary @cards.json")

	local("curl -X POST http://localhost:9200/spoils/_refresh")

@task
def boot_application():
	with path("$HOME/npm/bin"):
		run("pm2 kill")
		run("pm2 start %s/startup.json" % APP_DIR)
		run("pm2 save")

@task
def stop_application():
	with path("$HOME/npm/bin"):
		sudo("pm2 stop all")

@task
def restart_application():
	with path("$HOME/npm/bin"):
		sudo("pm2 restart all")

@task
def update_application(update_database = True):
	maintenance_up()
	stop_application()

	with cd(APP_DIR):
		run("git fetch --all")
		run("git reset --hard origin/master")

	install_npm_packages()
	build_client()

	if update_database == True:
		update_remote_database()

	restart_application()
	maintenance_down()

@task
def maintenance_up():
	with cd("%s/client" % APP_DIR):
		run("mv maintenance_down.html maintenance.html")

@task
def maintenance_down():
	with cd("%s/client" % APP_DIR):
		run("mv maintenance.html maintenance_down.html")

@task
def queries(n = 20):
	run("tail -n %d queries.log" % int(n))

@task
def client_errors(n = 20):
	run("tail -n %d client-errors.log" % int(n))
