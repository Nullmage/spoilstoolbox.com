import glob, json, os, zipfile

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))
CLIENT_FILES_PATH = os.path.join(SCRIPT_PATH, "..", "..", "client", "files")

GAME_ID = "844d5fe3-bdb5-4ad2-ba83-88c2c2db6d88"

class OctgnImagePacker:
	def run(self):
		for set_dir in glob.glob(os.path.join(SCRIPT_PATH, "..", "cards", "sets", "*")):
			self.create_pack(set_dir)
	
	def create_pack(self, set_dir):
		print("Processing set '%s'" % os.path.basename(set_dir))

		with open(os.path.join(set_dir, "set.json"), "r", encoding = "utf-8") as f:
			set_content = json.load(f)
			set_id, set_name = set_content["id"], set_content["name"]
			pack_path = os.path.join(SCRIPT_PATH, GAME_ID, "Sets", set_id, "Cards")

			try:
				os.makedirs(os.path.dirname(pack_path))
				os.symlink(set_dir, pack_path)
			except:
				pass

			print("Compressing...")
			self.compress_pack(set_name, pack_path)

	def compress_pack(self, set_name, pack_path):
		with zipfile.ZipFile(os.path.join(CLIENT_FILES_PATH, "%s.o8c" % set_name), "w") as pack:
			for image in glob.glob(os.path.join(pack_path, "*.jpg")):
				pack.write(image)

OctgnImagePacker().run()