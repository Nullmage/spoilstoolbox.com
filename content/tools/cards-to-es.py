import json, glob, os
from card_text_parser import parse_card_text

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))
CARDS_ROOT = os.path.join(SCRIPT_PATH, "..", "cards")

BLOCK_SETS = [
	"b553d26a-a953-4b49-9d97-682f3a48114c", # Shade
	"74be93a3-2f0f-44cc-9162-fbd6e5b42ef5", # Holy Heist
	"2fc61d6d-a080-4f4c-956b-8d5e518eeb76", # Ungodly Mess
]

def is_block_legal(sets):
	return len([s["id"] for s in sets if s["id"] in BLOCK_SETS]) > 0

CURRENT_SETS = [
	"b14b4b95-77aa-4f69-96b6-ca09b77ad946", # 2nd Edition
	"b6c94ada-f56e-41e5-80d0-c90186e23459", # Seed 3
	"b553d26a-a953-4b49-9d97-682f3a48114c", # Shade
	"74be93a3-2f0f-44cc-9162-fbd6e5b42ef5", # Holy Heist
	"9387948c-0356-40a8-897d-16978ff9fea9", # Seed Saga
	"2fc61d6d-a080-4f4c-956b-8d5e518eeb76", # Ungodly Mess
]

def is_current_legal(sets):
	return len([s["id"] for s in sets if s["id"] in CURRENT_SETS]) > 0

def is_chronicles_legal(sets):
	return True

FORMATS = [{
	"name": "Block",
	"check": is_block_legal
}, {
	"name": "Current",
	"check": is_current_legal
}, {
	"name": "Chronicles",
	"check": is_chronicles_legal
}]

class Exporter:
	def run(self):
		cards = []

		print("Loading card texts...")
		self._card_texts = self.load_card_texts()

		print("Parsing cards...")
		for set_dir in glob.glob(os.path.join(CARDS_ROOT, "sets", "*")):
			print("- Parsing set '%s'" % set_dir)
			set_file_name = os.path.join(set_dir, "set.json")
			cards += self.parse_set(set_file_name)

		print("Loading restricted list...")
		restricted_cards = self.load_restricted_list()

		print("Calculating format legality...")
		self.calculate_format_legality(cards, restricted_cards)

		print("Writing cards to disc...")
		self.write(cards)

	def load_card_texts(self):
		with open(os.path.join(CARDS_ROOT, "card-texts.json"), "r", encoding = "utf-8") as f:
			return json.load(f)

	def parse_set(self, set_file_name):
		with open(set_file_name, "r", encoding = "utf-8") as set_file:
			cards = []
			set_content = json.load(set_file)
			set_info = {
				"id": set_content["id"],
				"order": set_content["order"],
				"name": set_content["name"]
			}

			for c in set_content["cards"]:
				card = self.parse_card(c)
				card["set"] = set_info
				cards.append(card)

			return cards

	def parse_card(self, card):
		card_text = self._card_texts[card["name"]]

		if card_text:
			card["text"] = card_text
			parsed_text = parse_card_text(card_text)

			if parsed_text:
				card["meta"] = parsed_text

		return card

	def load_restricted_list(self):
		with open(os.path.join(CARDS_ROOT, "restricted-cards.json"), "r", encoding = "utf-8") as f:
			return json.load(f)

	def calculate_format_legality(self, cards, restricted_cards):
		cards_by_name = {}

		for card in cards:
			if not card.get("alt-art", False):
				cards_by_name.setdefault(card["name"], []).append(card["set"])

		for card in cards:
			card_name = card["name"]
			sets = cards_by_name[card_name]

			for entry in FORMATS:
				format_name = entry["name"]
				format_check = entry["check"]

				banned = card_name in restricted_cards["bans"][format_name]
				special = card_name in restricted_cards["special"]
				legal = "Banned" if banned else "Special" if special else "Yes" if format_check(sets) else "No"

				card.setdefault("format", []).append({
					"name": format_name,
					"legal": legal
				})

	def write(self, cards):
		with open("cards.json", "w", encoding = "utf-8") as out:
			for card in cards:
				out.write('{ "index": { "_id": "%s" } }\n' % card["id"])
				out.write(json.dumps(card, sort_keys = True) + "\n")
	
Exporter().run()
