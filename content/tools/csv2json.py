import sys, csv, json, itertools, traceback

cards = []

THRESHOLD_ICONS = {
	"arcanist":		"o",
	"banker":		"g",
	"gearsmith":	"e",
	"rogue":		"d",
	"warlord":		"r",
	"universal":	"v"
}

in_file = sys.argv[1]
out_file = sys.argv[2]

def parse_card(card):
	return {
		"name": card_name(card),
		"cost": card_cost(card),
		"threshold": card_threshold(card),
		"rarity": card_rarity(card),
		"trade": card_trade(card),
		"type": card_type(card),
		"subtype": card_subtype(card),
		"strength": card_strength(card),
		"life": card_life(card),
		"speed": card_speed(card),
		"structure": card_structure(card)
	}

def card_name(card):
	return card[0]

def card_cost(card):
	return int(card[1])

def card_threshold(card):
	return THRESHOLD_ICONS[card[4]] * int(card[2])

def card_rarity(card):
	return card[3]

def card_trade(card):
	return card[4]

def card_type(card):
	return card[5]

def card_subtype(card):
	subtypes = [subtype.strip() for subtype in card[6].split(",") if subtype] 
	return subtypes if len(subtypes) > 0 else None

def card_strength(card):
	return int(card[7]) if card[7] != "" else None

def card_life(card):
	return int(card[8]) if card[8] != "" else None

def card_speed(card):
	return int(card[9]) if card[9] != "" else None

def card_structure(card):
	return int(card[10]) if card[10] != "" else None

def remove_none(card):
	return { k: v for k, v in card.items() if v is not None }

with open(in_file, "r", encoding = "utf-8") as csv_file:
	for card in itertools.islice(csv.reader(csv_file), 1, None):
		try:
			parsed = remove_none(parse_card(card))
			cards.append(parsed)
		except Exception as e:
			print("Error while processing", card)
			traceback.print_exc(e)
			exit(1)
	
	with open(out_file, "w", encoding = "utf-8") as json_file:
		json_file.write(json.dumps(cards, sort_keys = True, indent = 4))
