import glob, os, json, hashlib, uuid
from xml.etree import cElementTree as ET
from xml.dom.minidom import parseString

GAME_ID = "844d5fe3-bdb5-4ad2-ba83-88c2c2db6d88"
VERSION = "1.0"
GAME_VERSION = "1.3.1"
PROPERTIES = ["rarity", "trade", "type", "subtype", "threshold", "cost", "strength", "life", "speed", "structure"]
DECLARATION = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>'

class Exporter:
	def run(self):
		print("Loading card texts...")
		self._card_texts = self.load_card_texts()

		for set_dir in glob.glob("../cards/sets/*"):
			print("Parsing %s..." % os.path.basename(set_dir))
			set_file_name = os.path.join(set_dir, "set.json")
			set_node = self.parse_set(set_file_name)

			print("Writing XML...")
			self.write_xml(set_node.attrib["name"], set_node)
		
		# print("Generating Factions and Resources pack...")
		# set_node = self.generate_factions_and_resources()

		# print("Writing XML...")
		# self.write_xml(set_node.attrib["name"], set_node)

	def load_card_texts(self):
		with open("../cards/card-texts.json", "r", encoding = "utf-8") as card_texts_file:
			return json.load(card_texts_file)

	def parse_set(self, set_file_path):
		set_node = ET.Element("set")

		with open(set_file_path, "r", encoding = "utf-8") as set_file:
			set_content = json.load(set_file)
			set_node.attrib["id"] = set_content["id"]
			set_node.attrib["name"] = set_content["name"]
			set_node.attrib["gameId"] = GAME_ID
			set_node.attrib["version"] = VERSION
			set_node.attrib["gameVersion"] = GAME_VERSION

			if set_content.get("packaging", None):
				self.parse_packaging(set_node, set_content)
			
			cards_node = ET.SubElement(set_node, "cards")

			for card in set_content["cards"]:
				self.parse_card(cards_node, card)

		return set_node

	def parse_packaging(self, parent, set_content):
		packaging_node = ET.SubElement(parent, "packaging")
		pack_node = ET.SubElement(packaging_node, "pack")
		pack_node.attrib["id"] = str(uuid.uuid3(uuid.NAMESPACE_URL, set_content["id"])) # Creates a deterministic UUID
		pack_node.attrib["name"] = "Booster"

		picks = [pick for pick in set_content["packaging"] if "probability" not in pick]
		options = [pick for pick in set_content["packaging"] if "probability" in pick]

		for pick in picks:
			pick_node = ET.SubElement(pack_node, "pick")
			pick_node.attrib["key"] = "Rarity"
			pick_node.attrib["value"] = pick["rarity"].capitalize()
			pick_node.attrib["qty"] = str(pick["qty"])
		
		if len(options) > 0:
			options_node = ET.SubElement(pack_node, "options")

			for option in options:
				option_node = ET.SubElement(options_node, "option")
				option_node.attrib["probability"] = str(option["probability"])
				pick_node = ET.SubElement(option_node, "pick")
				pick_node.attrib["key"] = "Rarity"
				pick_node.attrib["value"] = option["rarity"].capitalize()
				pick_node.attrib["qty"] = str(option["qty"])

	def parse_card(self, parent, card):
		card_node = ET.SubElement(parent, "card")
		card_node.attrib["id"] = card["id"]
		card_node.attrib["name"] = card["name"]

		for prop_name in PROPERTIES:
			self.parse_property(card_node, card, prop_name)
		
		card_text_node = ET.SubElement(card_node, "property")
		card_text_node.attrib["name"] = "Text"
		card_text_node.attrib["value"] = self._card_texts[card["name"]].replace("\n", "&#10;")

		return card_node

	def parse_property(self, parent, card, prop_name):
		prop_element = ET.SubElement(parent, "property")
		prop_element.attrib["name"] = prop_name.capitalize()

		if prop_name == "subtype":
			prop_element.attrib["value"] = ", ".join(card.get(prop_name, []))
		elif prop_name == "threshold":
			prop_element.attrib["value"] = card.get(prop_name, "").upper()
		elif prop_name == "trade" and card["type"] == "faction":
			prop_element.attrib["value"] = "Universal"
		else:
			prop_element.attrib["value"] = str(card.get(prop_name, "")).capitalize()

	def write_xml(self, set_name, root_element):
		raw_xml = ET.tostring(root_element, encoding = "utf-8")
		pretty_xml = parseString(raw_xml).toprettyxml(indent = "\t")
		pretty_xml = pretty_xml.replace('<?xml version="1.0" ?>', "")

		set_path = os.path.join("OCTGN", "sets", set_name)

		try:
			os.makedirs(set_path)
		except:
			pass

		with open(os.path.join(set_path, "set.xml"), "w", encoding = "utf-8") as set_file:
			set_file.write(DECLARATION + "\n")
			set_file.write(pretty_xml)
	
	def generate_factions_and_resources(self):
		def faction_or_resource(card):
			return card["type"] == "faction" or card.get("supertype") == "staple"
		
		set_node = ET.Element("set")
		set_node.attrib["id"] = str(uuid.uuid3(uuid.NAMESPACE_URL, "Factions and Resources"))
		set_node.attrib["name"] = "Factions and Resources"
		set_node.attrib["gameId"] = GAME_ID
		set_node.attrib["version"] = VERSION
		set_node.attrib["gameVersion"] = GAME_VERSION

		packaging_node = ET.SubElement(set_node, "packaging")
		pack_node = ET.SubElement(packaging_node, "pack")
		pack_node.attrib["id"] = str(uuid.uuid3(uuid.NAMESPACE_URL, set_node.attrib["id"]))
		pack_node.attrib["name"] = "Booster"
		pick_faction_node = ET.SubElement(pack_node, "pick")
		pick_faction_node.attrib["key"] = "Type"
		pick_faction_node.attrib["value"] = "Faction"
		pick_faction_node.attrib["qty"] = "1"
		pick_resource_node = ET.SubElement(pack_node, "pick")
		pick_resource_node.attrib["key"] = "Type"
		pick_resource_node.attrib["value"] = "Resource"
		pick_resource_node.attrib["qty"] = "unlimited"

		cards_node = ET.SubElement(set_node, "cards")

		for set_dir in glob.glob("../cards/sets/*"):
			with open("%s/set.json" % set_dir, encoding = "utf-8") as set_file:
				set_content = json.load(set_file)
				factions_and_resources = [card for card in set_content["cards"] if faction_or_resource(card)]
				
				for card in factions_and_resources:
					card_node = self.parse_card(cards_node, card)

					# We need to generate a new deterministic ID so it doesn't collide with the existing ones 
					card_node.attrib["id"] = str(uuid.uuid3(uuid.NAMESPACE_URL, card["id"]))
		
		return set_node

Exporter().run()
