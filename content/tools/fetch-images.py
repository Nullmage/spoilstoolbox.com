# Tool for downloading card images from: http://clockman-card-vault.goodsie.com/

# $("img.js-module-products-image").map((i, el) => {
# 	var e = $(el);

# 	return {
# 		"name": e.attr("alt"),
# 		"url": e.data("src")
# 	}
# }).each((i, image) => {
# 	console.log(`{ "name": "${image.name}", "url": "http:${image.url}" },\n`);
# });

# WIDTH_RATIO = 25 / 500
# HEIGTH_RATIO = 25 / 700

import argparse, os, json, urllib.request, glob
from PIL import Image

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--crop", default = False, type = int, nargs = 4, help = "If set, crops all images after download using the bounding box provided")
parser.add_argument("-o", "--output", default = "temp", help = "Output directory")
args = parser.parse_args()

try:
	os.makedirs(args.output)
except:
	pass

def download(card):
	print("Downloading %s.jpg..." % card["name"])
	output = "%s.jpg" % os.path.join(args.output, card["name"])
	urllib.request.urlretrieve(card["url"], output)

def crop(file_path):
	Image.open(file_path).crop(args.crop).save(file_path, quality = 95)

with open("fetch-images.json", "r") as cards:
	for card in json.load(cards):
		download(card)

if args.crop:
	for file_path in glob.glob("%s/*.jpg" % args.output):
		print("Cropping %s.jpg" % file_path)
		crop(file_path)

print("...Done!")
