import os
from subprocess import call

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))

call("curl -X DELETE http://localhost:9200/spoils", shell = True)

SETTINGS_PATH = os.path.join(SCRIPT_PATH, "elasticsearch", "settings.json")
call("curl -X PUT http://localhost:9200/spoils -d @%s" % SETTINGS_PATH, shell = True)
call("curl -X DELETE http://localhost:9200/spoils/card", shell = True)

CARD_MAPPING = os.path.join(SCRIPT_PATH, "elasticsearch", "card-mapping.json")
call("curl -X PUT http://localhost:9200/spoils/_mapping/card -d @%s" % CARD_MAPPING, shell = True)

CARDS = os.path.join(SCRIPT_PATH, "cards.json")
call("curl -X POST http://localhost:9200/spoils/card/_bulk --data-binary @%s" % CARDS, shell = True)
call("curl -X POST http://localhost:9200/spoils/_refresh", shell = True)
