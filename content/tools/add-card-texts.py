import sys, csv, json, itertools

new_cards = {}
all_cards = {}

in_file = sys.argv[1]
out_file = sys.argv[2]

with open(in_file, "r", encoding = "utf-8") as csv_file:
	for card in itertools.islice(csv.reader(csv_file), 1, None):
		new_cards[card[0]] = card[1]

with open(out_file, "r", encoding = "utf-8") as json_file:
	current_cards = json.loads(json_file.read())
	all_cards = {**new_cards, **current_cards}

with open(out_file, "w", encoding = "utf-8") as json_file:
	json.dump(all_cards, json_file, sort_keys = True, indent = 4)
