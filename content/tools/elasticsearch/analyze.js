"use strict";

const request = require("request");
const qs = require("../server/models/card-query-parser");
const argv = require("minimist")(process.argv.slice(2), {
	"default": { "a": "standard" }
});

if (argv.p) {
	const es_query = qs.parse({ "query_string": argv.p });
	const query_string = es_query.text.join(" AND ");
	analyze(query_string, argv.a);
} else {
	analyze(argv._[0], argv.a);
}

function analyze(query_string, analyzer) {
	request.post({
		"url": "http://localhost:9200/_analyze",
		"form": `{
			"analyzer": "${analyzer}",
			"text": "${query_string}"
		}`
	}, (err, response, body) => {
		if (err) {
			console.error(err);
		} else {
			const json = JSON.parse(body).tokens.map(t => t.token);
			console.log(json);
		}
	});
}
