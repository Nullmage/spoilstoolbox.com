"use strict";

const test = require("tape");
const Browser = require("nightmare");

const main = require("../../server/main");
const common = require("./common");
const actions = require("./card-search-actions");
const Lookup = require("../../client/js/config/messages");
const L = function(code, ...args) {
	const message = Lookup(code, args);
	return typeof(message) === "string" ? message : message.text;
};

const BROWSER_OPTIONS = { "show": true };

test("INIT", t => {
	main.init(() => {
		Browser(BROWSER_OPTIONS).cookies.clear().run(() => t.end()).end();
	});
});

test("Make a search that returns an empty set", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.search("threshold = 1337"))
		.use(common.run(result => {
			t.equal(result.num_cards, 0);
			t.equal(result.notice, L("CARD_SEARCH.EMPTY"));
			t.end();
		})).end();
});

test("Make a regular search", t => {
	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.search("arcanist character threshold = 4"))
		.use(common.run(result => {
			t.assert(result.num_cards > 0);
			t.equal(result.notice, L("CARD_SEARCH.RESULT", result.num_cards));
			t.end();
		})).end();
});

test("Make a search and open card info modal", t => {
	const CARD_NAME = "Hideous Hexapede of Horror";

	Browser(BROWSER_OPTIONS)
		.use(common.goto("/"))
		.use(actions.search(CARD_NAME, false))
		.use(actions.open_card_modal())
		.use(common.run(result => {
			t.equal(result.title, CARD_NAME);
			t.end();
		})).end();
});

test("SHUTDOWN", t => {
	main.shutdown(() => {
		t.end();
	});
});