/* eslint-disable */

"use strict";

const common = require("./common");

const BUTTONS = {
	"SIGN_IN":			"#sign-in-button",
	"REGISTER":			"#register-button",
	"RECOVER_ACCOUNT":	"#recover-account-button"
};

const FORMS = {
	"REGISTRATION":		"#registration-form",
	"SIGN_IN":			"#sign-in-form",
	"RECOVER":			"#recover-account-form",
	"CHANGE_PASSWORD":	"#change-password-form",
	"SET_PASSWORD":		"#set-password-form"
};

exports.register_account = function(email, username, password) {
	return function(browser) {
		browser
			.click(BUTTONS.REGISTER)
			.wait(common.visible, FORMS.REGISTRATION)
			.use(common.submit_form(FORMS.REGISTRATION, {
				"email": email,
				"username": username,
				"password": password
			}))
			.use(common.collect_response());
	};
};

exports.activate_account = function(token) {
	return function(browser) {
		browser
			.use(common.goto(`/account/activate/${token}`))
			.use(common.collect_response())
	};
};

exports.sign_in = function(username, password) {
	return function(browser) {
		browser
			.click(BUTTONS.SIGN_IN)
			.wait(common.visible, FORMS.SIGN_IN)
			.use(common.submit_form(FORMS.SIGN_IN, {
				"username": username,
				"password": password
			}))
			.use(common.collect_response());
	};
};

exports.change_password = function(current_password, new_password) {
	return function(browser) {
		browser
			.use(common.goto("/account/settings"))
			.wait(common.visible, FORMS.CHANGE_PASSWORD)
			.use(common.submit_form(FORMS.CHANGE_PASSWORD, {
				"current_password": current_password,
				"new_password": new_password
			}))
			.use(common.collect_response());
	};
};

exports.recover_account = function(email_or_username) {
	return function(browser) {
		browser
			.click(BUTTONS.SIGN_IN)
			.wait(common.visible, FORMS.SIGN_IN)
			.click(BUTTONS.RECOVER_ACCOUNT)
			.wait(common.visible, FORMS.RECOVER)
			.use(common.submit_form(FORMS.RECOVER, {
				"email_or_username": email_or_username
			}))
			.use(common.collect_response());
	};
};

exports.set_password = function(token, password) {
	return function(browser) {
		browser
			.use(common.goto(`/account/set-password/${token}`))
			.wait(common.visible, FORMS.SET_PASSWORD)
			.use(common.submit_form(FORMS.SET_PASSWORD, {
				"password": password
			}))
			.use(common.collect_response());
	};
};
