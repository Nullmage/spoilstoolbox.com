/* eslint-disable */

"use strict";

const common = require("./common");

const FORM = "#card-search-form";

exports.search = function(query_string, collect = true) {
	return function(browser) {
		browser.use(common.submit_form(FORM, {
			"query_string": query_string
		}));

		if (collect) {
			browser.evaluate(NOTICE_SELECTOR => {
				var result = {
					"num_cards": $("img.card-image").length,
					"notice": $(NOTICE_SELECTOR).html()
				};

				return result;
			}, common.NOTICE_SELECTOR);
		}
	}
};

exports.open_card_modal = function() {
	return function(browser) {
		browser
			.click("img.card-image")
			.wait(common.visible, "#card-modal")
			.evaluate(function() {
				return {
					"title": $("#card-modal .modal-title").text()
				};
			});
	}
};

function collect_response() {
	return function(browser) {
		browser.evaluate(NOTICE_SELECTOR => {
			var result = {
				"num_cards": $("img.card-image").length,
				"notice": $(NOTICE_SELECTOR).html()
			};

			return result;
		}, common.NOTICE_SELECTOR);
	}
}
