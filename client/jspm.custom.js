SystemJS.config({
	production: ENVS.prod
});

var system_normalize = System.instantiate;
System.instantiate = function() {
	NProgress.inc();
	return system_normalize.apply(this, arguments);
}

var system_locate = System.locate;
System.locate = function(load) {
	return Promise.resolve(system_locate.call(this, load)).then(function(address) {
		// Same as address.startsWith(...)
		if (!address.lastIndexOf("https", 0) === 0) {
			address += "?" + RND;
		}
		
		if (!ENVS.prod) {
			address = address.replace(".min", "");
		}
		
		return address;
	});
}
