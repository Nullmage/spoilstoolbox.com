import $ from "jquery";
import "block-ui";

import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";

export default {
	"route": {
		"data": function(t) {
			t.next({ "token": t.to.params.token });
		}
	},
	
	"ready": ready,
	"template": "<div></div>",
	"data": function() {
		return {
			"token": null
		};
	}
};

function ready() {
	$.blockUI({
		// Make sure we cover bootstrap's navbar
		"baseZ": 9999,
		
		"css": {
			"border": "none",
			"padding": "15px",
			"backgroundColor": "#000",
			"-webkit-border-radius": "10px",
			"-moz-border-radius": "10px",
			"border-radius": "10px",
			"opacity": 0.5,
			"color": "#fff"
		},
		
		"message": "<h4>Activating your account...<span class='fa fa-spinner fa-spin'></span></h4>"
	});
	
	$.ajax({
		"url": "/account/activate",
		"method": "PATCH",
		"data": { "token": this.token }
	}).always(() => {
		$.unblockUI();
		this.$router.go("/");
	}).done(() => {
		display(L("ACCOUNT.ACTIVATED"));
	}).fail(() => {
		display(L("ACCOUNT.ACTIVATION_FAILED"));
	});
}
