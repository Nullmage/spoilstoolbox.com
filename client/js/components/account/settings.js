import $ from "jquery";

import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";
import { Session } from "js/lib/storage.js";
import Form from "js/components/shared/form.js";

import Template from "./settings.html";

export default {
	"route": {
		"canActivate": function(t) {
			$.getJSON("/account/settings").done(settings => {
				Session.local = settings;
				t.next();
			}).fail(() => t.abort());
		},
		
		"data": t => t.next(Session.local)
	},
	
	"ready": ready,
	"template": Template,
	"mixins": [Form],
	"data": function() {
		return {
			"email": null,
			"username": null
		};
	}
};

function ready() {
	this.$on("form-success", () => {
		display(L("ACCOUNT.PASSWORD_CHANGED"));
		this.reset_form();
	});
}