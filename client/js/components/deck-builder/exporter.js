import $ from "jquery";
import { stack_cards, sort_stacks } from "js/lib/filters.js";

export { to_text, to_octgn, to_database };

function to_text(faction, cards) {
	function process_cards(content, title, cards) {
		function header(h) {
			return "\r\n" + h + "\r\n";
		}

		function enumerate_stacks(stacks) {
			let result = "";

			stacks.forEach(stack => {
				const tab = stack.copies <= 9 ? "  " : " ";
				result += stack.copies + tab + stack.card.name + "\r\n";
			});

			return result;
		}

		if (cards.length > 0) {
			const stacks = sort_stacks(stack_cards(cards));
			
			content.push(header(title + " (" + stacks.length + ")"));
			content.push(enumerate_stacks(stacks));
		}
	}

	const content = [];

	const faction_name = faction ? faction.name : "None";
	content.push("Faction: " + faction_name + "\r\n");

	const resources = cards.filter(card => card.type === "resource");
	process_cards(content, "Resources", resources);

	const characters = cards.filter(card => card.type === "character");
	process_cards(content, "Characters", characters);

	const tactics = cards.filter(card => card.type === "tactic");
	process_cards(content, "Tactics", tactics);

	const items = cards.filter(card => card.type === "item");
	process_cards(content, "Items", items);

	const locations = cards.filter(card => card.type === "location");
	process_cards(content, "Locations", locations);

	return content;
}

function to_octgn(faction, starting_resources, cards) {
	const content = [];

	content.push('<?xml version="1.0" encoding="utf-8" standalone="yes"?>\r\n');
	content.push('<deck game="844d5fe3-bdb5-4ad2-ba83-88c2c2db6d88">\r\n');
	content.push('\t<section name="Game Deck" shared="False">\r\n');

	// We need to remove the starting resources from the deck so they don't get included
	// twice. But, the proper solution would be to modify the OCTGN game code so that players
	// choose their starting resources when they begin a game instead of saving it in the file.
	const cards_without_starting_resources = cards.filter(card => !is_starting_resource(card));
	
	function is_starting_resource(card) {
		for (let i = 0; i < starting_resources.length; i++) {
			const starting_resource = starting_resources[i];
			
			if (card.id === starting_resource.id && !starting_resource.marked) {
				starting_resource.marked = true;
				return true;
			}
		}
	}

	const stacks = sort_stacks(stack_cards(cards_without_starting_resources));
	stacks.forEach(stack => {
		content.push('\t\t<card qty="' + stack.copies + '" id="' + stack.card.id + '">' + stack.card.name + '</card>\r\n');
	});

	content.push('\t</section>\r\n');
	content.push('\t<section name="Faction" shared="False">\r\n');

	if (faction) {
		content.push('\t\t<card qty="1" id="' + faction.id + '">' + faction.name + '</card>\r\n');
	}

	const stacked_starting_resources = sort_stacks(stack_cards(starting_resources));
	stacked_starting_resources.forEach(stack => {
		content.push('\t\t<card qty="' + stack.copies + '" id="' + stack.card.id + '">' + stack.card.name + '</card>\r\n');
	});

	content.push('\t</section>\r\n');
	content.push('</deck>\r\n');

	return content;
}

function to_database(name, cards) {
	function max_threshold(t1, t2) {
		if (t1 && t2) {
			return t1.length > t2.length ? t1 : t2;
		} else if (t1) {
			return t1;
		} else if (t2) {
			return t2;
		}
	}
	
	const thresholds = {};

	const card_ids = cards.map(card => {
		const trade = card.trade;
		const thresh = card.threshold;
		
		if (trade) {
			thresholds[trade] = max_threshold(thresholds[trade], thresh);
		}
		
		return card.id;
	});
	
	const threshold = $.map(thresholds, (thresh, trade) => {
		return { "trade": trade, "threshold": thresh };
	}).sort((e1, e2) => {
		return e1.trade.localeCompare(e2.trade);
	}).map(entry => {
		return entry.threshold;
	}).join("");
	
	return {
		"name": name,
		"cards": card_ids,
		"threshold": threshold
	};
}
