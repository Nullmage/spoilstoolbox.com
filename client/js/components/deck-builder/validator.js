import { stack_cards } from "js/lib/filters.js";

export { validate };

function validate(faction, cards) {
	let errors = [];

	if (cards.length < 75) {
		errors.push("Your deck must have at least 75 cards.");
	}

	if (!faction) {
		errors.push("You must choose a faction card.");
	} else {
		const staples = cards.filter(card => card.supertype === "staple");
		errors = errors.concat(validate_resources(faction, staples));
	}

	const stacks = stack_cards(cards);
	errors = errors.concat(validate_stacks(stacks));

	// Remove 'undefined' values which can happen when no errors
	// are found when validating resources.
	return errors.filter(Boolean);
}

function validate_resources(faction, staples) {
	switch (faction.name) {
		case "Drowned Catacombs":
			if (count_by(staples, "name", "Deception") < 1) {
				return error_require("Deception", 1);
			} break;
		case "Order of the Golden String":
			if (count_by(staples, "name", "Greed") < 1) {
				return error_require("Greed", 1);
			} break;
		case "Hall of Great Justice":
			if (count_by(staples, "name", "Rage") < 1) {
				return error_require("Rage", 1);
			} break;
		case "Moist Cave of the Darkpump":
		case "The Primitive Tribes of Thabbash":
		case "Lugubrian Council":
			if (count_by(staples, "name", "Obsession") < 1) {
				return error_require("Obsession", 1);
			} break;
		case "Alliance of Handy Weirdos":
			if (count_by(staples, "name", "Elitism") < 1) {
				return error_require("Elitism", 1);
			} break;
		case "Unlikely Heroes":
			if (unique_by(staples, "name").length < 2) {
				return error_unique(2);
			} break;
		case "The Tournament Faction":
			if (staples.length < 2) {
				return error_require("staple resources", 2);
			} break;
	}
}

function count_by(cards, prop, value) {
	return cards.filter(card => card[prop] === value).length;
}

function unique_by(cards, prop) {
	return cards.filter((seen, card) => {
		const prop_value = card[prop];

		if (!seen[prop_value]) {
			seen[prop_value] = true;
			return card;
		}
	}, []);
}

function error_require(card_name, copies) {
	return `Your faction requires your deck to contain at least ${copies} '${card_name}'.`;
}

function error_unique(copies) {
	return `Your faction requires your deck to contain at least ${copies} different resources.`;
}

function validate_stacks(stacks) {
	const errors = [];

	stacks.forEach(function(stack) {
		const card = stack.card;

		if (!validate_legality(card.format)) {
			errors.push(`'${card.name}' is not legal in any format.`);
		}

		if (!validate_copies(stack)) {
			errors.push(`Your deck may not contain more than 4 copies of '${card.name}'.`);
		}
	});

	return errors;
}

function validate_legality(formats) {
	for (const i in formats) {
		if (formats[i].legal === "Yes") {
			return true;
		}
	}
}

function validate_copies(stack) {
	const is_staple = stack.card.supertype === "staple";
	const is_party_clowns = stack.card.name === "Party Clowns";

	return is_staple || is_party_clowns || stack.copies <= 4;
}
