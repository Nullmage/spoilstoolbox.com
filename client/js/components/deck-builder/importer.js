import $ from "jquery";

export { from_database, from_file };

function from_database(id) {
	return new Promise((resolve, reject) => {
		$.getJSON(`/decks/${id}`)
		.done(deck => resolve(deck))
		.fail(xhr => reject(xhr));
	});
}

function from_file(file) {
	return new Promise((resolve, reject) => {
		const extension = file.name.split(".")[1];
		const file_reader = new FileReader();
		
		file_reader.onload = function() {
			$.post("/deck-builder/parse_from_file", {
				"content": file_reader.result,
				"extension": extension
			})
			.done(cards => resolve(cards))
			.fail(xhr => reject(xhr.responseJSON));
		};
		
		file_reader.readAsText(file);
	});
}
