import Highcharts from "highcharts";

export { init, update, clear };

let cost_chart, threshold_chart, trade_chart, type_chart;

function init() {
	cost_chart = create_column_chart("cost-chart", "Costs");
	threshold_chart = create_column_chart("threshold-chart", "Thresholds");
	trade_chart = create_pie_chart("trade-chart", "Trades");
	type_chart = create_pie_chart("type-chart", "Types");
}

function create_column_chart(element, title) {
	return new Highcharts.Chart({
		"chart": {
			"renderTo": element,
			"type": "column",
			"spacingRight": 0,
			"spacingLeft": 0
		},

		"title": {
			"text": title
		},
		
		"xAxis": {
			"allowDecimals": false,
			"categories": []
		},
		
		"yAxis": {
			"title": null,
			"allowDecimals": false,
			"labels": {
				"enabled": false
			},
			"stackLabels": {
				"enabled": true
			}
		},
		
		"legend": {
			"enabled": false
		},
		
		"exporting": {
			"enabled": false
		},
		
		"tooltip": {
			"style": { "text-transform": "capitalize" },
			"formatter": function() {
				return this.y !== 0 ? this.series.name : null;
			}
		},
		
		"plotOptions": {
			"column": {
				"stacking": "normal",
				"dataLabels": {
					"enabled": true,
					"color": "white",
					"allowOverlap": true,
					"formatter": function() {
						return this.y !== 0 ? this.y : null;
					}
				}
			}
		},

		"series": []
	});
}

function create_pie_chart(element, title) {
	return new Highcharts.Chart({
		"chart": {
			"renderTo": element,
			"type": "pie",
			"spacingRight": 0,
			"spacingLeft": 0
		},

		"title": {
			"text": title
		},
		
		"tooltip": {
			"style": { "text-transform": "capitalize" },
			"formatter": function() {
				return `${this.key}: ${this.y}`;
			}
		},
		
		"plotOptions": {
			"pie": {
				"showInLegend": true,
				"dataLabels": {
					"enabled": false
				}
			}
		},

		"legend": {
			"enabled": true,
			"useHTML": true,
			"style": { "text-transform": "capitalize" },
			"labelFormatter": function() {
				return `<div style="text-transform: capitalize">${this.name}: ${this.y}</div>`;
			}
		},

		"series": [{ "type": "pie" }]
	});
}

function update(cards) {
	const cost_data = group_chart_data(cards, "trade", "cost");
	update_column_chart(cost_chart, cost_data);

	const threshold_data = group_chart_data(cards, "trade", "threshold", v => v.length);
	update_column_chart(threshold_chart, threshold_data);

	const trade_data = group_chart_data(cards, "trade", "trade");
	update_pie_chart(trade_chart, trade_data);

	const type_data = group_chart_data(cards, "type", "type");
	update_pie_chart(type_chart, type_data);
}

const GROUP_TO_COLOR = {
	"arcanist":		"#8b649b",
	"tactic":		"#8b649b",
	"banker":		"#d39f28",
	"resource":		"#d39f28",
	"rogue":		"#8c2522",
	"location":		"#8c2522",
	"warlord":		"#5181c2",
	"character":	"#5181c2",
	"gearsmith":	"#969696",
	"item":			"#969696",
	"universal":	"#62817c"
};

function group_chart_data(cards, prop1_name, prop2_name, prop2_transform = v => v) {
	let x = {};
	const y = [];
	const aggregated = {};

	cards.forEach(card => {
		if (card[prop2_name]) {
			const prop2_value = prop2_transform(card[prop2_name]);
			x[prop2_value] = true;

			const prop1_value = card[prop1_name];
			aggregated[prop1_value] = aggregated[prop1_value] || {};
			aggregated[prop1_value][prop2_value] = aggregated[prop1_value][prop2_value] || 0;
			aggregated[prop1_value][prop2_value]++;
		}
	});
	
	x = Object.keys(x);

	for (let prop1_name in aggregated) {
		const data = [];
		const values = aggregated[prop1_name];

		for (let i in x) {
			data.push(values[x[i]] || 0);
		}

		y.push({
			"name": prop1_name,
			"data": data,
			"color": GROUP_TO_COLOR[prop1_name]
		});
	}

	return { "x": x, "y": y	};
}

function update_column_chart(chart, data) {
	function same_series(old_series, new_series) {
		if (old_series.length !== new_series.length) {
			return false;
		}

		for (let i = 0, size = new_series.length; i < size; ++i) {
			if (old_series[i].name !== new_series[i].name) {
				return false;
			}
		}

		return true;
	}

	if (chart.xAxis[0].categories.length !== data.x.length) {
		chart.xAxis[0].setCategories(data.x, false);
	}

	if (same_series(chart.series, data.y)) {
		for (let i in data.y) {
			chart.series[i].setData(data.y[i].data, false);
		}
	} else {
		clear_column_chart(chart);

		for (let i in data.y) {
			chart.addSeries(data.y[i], false);
		}
	}

	chart.redraw();
}

function update_pie_chart(chart, data) {
	const series_data = [];

	for (let i in data.y) {
		const name = data.y[i].name;
		const color = data.y[i].color;
		const values = data.y[i].data[i];

		series_data.push({ "name": name, "color": color, "y": values });
	}

	chart.series[0].setData(series_data);
}

function clear() {
	clear_column_chart(cost_chart);
	clear_column_chart(threshold_chart);
}

function clear_column_chart(chart) {
	while (chart.series.length > 0) {
		chart.series[0].remove(false);
	}
}
