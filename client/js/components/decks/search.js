import "selectize";
import $ from "jquery";

import { Local } from "js/lib/storage.js";
import Form from "js/components/shared/form.js";

import Navigation from "./navigation.js";
import Template from "./search.html";

export default {
	"route": {
		"activate": function(t) {
			if (!Local.has("cards")) {
				$.post("/cards/search", {
					"params": {
						"projection": ["id", "name"]
					}
				}, cards => {
					Local.set("cards", cards);
					t.next();
				});
			} else {
				t.next();
			}
		}
	},
	"ready": ready,
	"template": Template,
	"mixins": [Form],
	"components": {
		"navigation": Navigation
	},
	
	"data": function() {
		return {
			"decks": []
		};
	}
};

function ready() {
	this.form.find("select[name='trades']").selectize({
		"plugins": ["remove_button"],
		"render": {
			"option": function(data, escape) {
				switch (data.text) {
					case "Arcanist":	return "<div><img src='/img/icons/arcanist.png'> " + escape(data.text) + "</div>";
					case "Banker":		return "<div><img src='/img/icons/banker.png'> " + escape(data.text) + "</div>";
					case "Gearsmith":	return "<div><img src='/img/icons/gearsmith.png'> " + escape(data.text) + "</div>";
					case "Rogue":		return "<div><img src='/img/icons/rogue.png'> " + escape(data.text) + "</div>";
					case "Warlord":		return "<div><img src='/img/icons/warlord.png'> " + escape(data.text) + "</div>";
					case "Universal":	return "<div><img src='/img/icons/universal.png'> " + escape(data.text) + "</div>";
					default:			return "<div>" + escape(data.text) + "</div>";
				}
			}
		}
	});
	
	this.form.find("input[name='cards']").selectize({
		"plugins": ["remove_button"],
		"persist": false,
		"delimiter": ",",
		"maxOptions": 10000,
		"openOnFocus": false,
		"options": Local.get("cards").map(card => {
			return { "text": card.name, "value": card.id };
		})
	});
	
	this.$on("form-success", decks => {
		this.decks = decks;
	});
}
