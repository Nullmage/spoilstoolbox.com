import $ from "jquery";

import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";
import { Session } from "js/lib/storage.js";
import Events from "js/lib/events.js";
import SubmitButton from "js/components/shared/submit-button.js";

import Navigation from "./navigation.js";
import ViewMore from "./view-more.js";
import Template from "./mine.html";

export default {
	"route": {
		"canActivate": function(t) {
			$.getJSON("/decks/mine").done(decks => {
				Session.local = { "decks": decks };
				t.next();
			}).fail(() => t.abort());
		},
		
		"data": t => t.next(Session.local)
	},
	
	"template": Template,
	"components": {
		"navigation": Navigation,
		"view-more": ViewMore,
		"submit-button": SubmitButton
	},
	
	"data": function() {
		return {
			"decks": []
		};
	},
	
	"methods": {
		"publish": publish,
		"unpublish": unpublish,
		"trash": trash,
		"viewmore": viewmore
	}
};

function publish(e, deck) {
	Events.emit("confirm", {
		"title": "Publish",
		"body": `Are you sure you want to publish <strong>${deck.name}</strong>?`,
		"btn_class": "btn-success",
		"btn_text": "Yes, Publish",
		"callback": () => {
			const button = e.target.__vue__ || e.target.__v_frag.vm;
			button.ajax({
				"url": `/decks/${deck.id}/publish`,
				"method": "PATCH"
			}).done(() => {
				const entry = this.decks.find(entry => {
					if (entry.id === deck.id) {
						return true;
					}
				});

				entry.published = true;
				display(L("DECK.PUBLISHED"));
			});
		}
	});
}

function unpublish(e, deck) {
	Events.emit("confirm", {
		"title": "Unpublish",
		"body": `Are you sure you want to unpublish <strong>${deck.name}</strong>?`,
		"btn_class": "btn-warning",
		"btn_text": "Yes, Unpublish",
		"callback": () => {
			const button = e.target.__vue__ || e.target.__v_frag.vm;
			button.ajax({
				"url": `/decks/${deck.id}/unpublish`,
				"method": "PATCH"
			}).done(() => {
				const entry = this.decks.find(entry => {
					if (entry.id === deck.id) {
						return true;
					}
				});

				entry.published = false;
				display(L("DECK.UNPUBLISHED"));
			});
		}
	});
}

function trash(e, deck) {
	Events.emit("confirm", {
		"title": "Trash",
		"body": `Are you sure you want to trash <strong>${deck.name}</strong>?`,
		"btn_class": "btn-danger",
		"btn_text": "Yes, Trash",
		"callback": () => {
			const button = e.target.__vue__ || e.target.__v_frag.vm;
			button.ajax({
				"url": `/decks/${deck.id}`,
				"method": "DELETE"
			}).done(() => {
				this.decks = this.decks.filter(entry => {
					return entry.id !== deck.id;
				});

				display(L("DECK.TRASHED"));
			});
		}
	});
}

function viewmore(decks) {
	this.decks = this.decks.concat(decks);
}