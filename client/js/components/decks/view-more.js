import $ from "jquery";

import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";

import Template from "./view-more.html";

export default {
	"props": ["path"],
	"template": Template,
	"ready": ready,
	"methods": {
		"viewmore": viewmore
	}
};

function ready() {
	this.page = 1;
}

function viewmore() {
	$.ajax({
		"url": this.path + "/" + this.page,
		"method": "GET"
	}).done(result => {
		if (result.length === 0) {
			display(L("VIEW_MORE.EMPTY"));
		} else {
			this.page++;
			this.$emit("result", result);
		}
	});
}
