import $ from "jquery";
import Vue from "vue";
import "jquery.lazyload";

import CardSearchForm from "./shared/card-search-form.js";

import Template from "./cards.html";
import CardModal from "./modals/card.js";

export default {
	"template": Template,
	"components": {
		"card-search-form": CardSearchForm,
		"card-modal": CardModal
	},
	
	"data": function() {
		return {
			"cards": []
		};
	},
	
	"methods": {
		"show": show,
		"card_search_result": card_search_result
	}
};

function show(card) {
	this.$refs.modal.set_card(card);
	this.$refs.modal.open();
}

function card_search_result(cards) {
	this.cards = cards;
	
	Vue.nextTick(() => {
		$("img.lazy").lazyLoadXT();
		$(window).trigger("resize");
	});
}