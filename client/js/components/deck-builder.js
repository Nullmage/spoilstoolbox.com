import $ from "jquery";
import save_as from "file-saver";

import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";
import { Local, Session } from "js/lib/storage.js";
import * as Notice from "js/lib/notice.js";
import Form from "./shared/form.js";
import CardSearchForm from "./shared/card-search-form.js";
import SubmitButton from "./shared/submit-button.js";

import * as Statistics from "./deck-builder/stats.js";
import * as Importer from "./deck-builder/importer.js";
import * as Exporter from "./deck-builder/exporter.js";
import * as Validator from "./deck-builder/validator.js";
import CardStack from "./deck-builder/card-stack.js";
import Template from "./deck-builder.html";

export default {
	"route": {
		"canActivate": function(t) {
			const id = t.to.params.id;
			
			if (id) {
				Importer.from_database(id).then(deck => {
					Session.local = deck;
					t.next();
				}).catch(xhr => {
					if (xhr.status === 404) {
						display(L("DECK.NOT_FOUND"));
					}
					
					t.abort();
				});
			} else {
				t.next();
			}
		},
		
		"data": t => t.next(Session.local)
	},
	"ready": ready,
	"template": Template,
	"mixins": [Form],
	"components": {
		"card-search-form": CardSearchForm,
		"card-stack": CardStack,
		"submit-button": SubmitButton
	},
	
	"data": function() {
		return init_data();
	},
	
	"computed": {
		"save_type": function() {
			if (!this.$root.user.authed || this.id === null) {
				return "create";
			} else if (this.$root.user.username === this.username) {
				return "update";
			} else {
				return "clone";
			}
		},
		"save_icon": function() {
			switch (this.save_type) {
				case "create":	return ["glyphicon", "glyphicon-save"];
				case "update":	return ["glyphicon", "glyphicon-save"];
				case "clone":	return ["fa", "fa-clone"];
			}
		},
		"save_text": function() {
			switch (this.save_type) {
				case "create":	return "Save";
				case "update":	return "Update";
				case "clone":	return "Clone";
			}
		},
		"save_disabled": function() {
			return !this.$root.user.authed;
		},
		"save_title": function() {
			return this.save_disabled ? "You need to be signed in to save your deck." : "";
		},
		"faction": function() {
			return this.cards.filter(card => card.type === "faction");
		},
		"faction_card": {
			"get": function() {
				return this.faction.shift();
			},
			"set": function(card) {
				this.cards.$remove(this.faction_card);
				this.cards.push(card);
			}
		},
		"resources": function() {
			return this.cards.filter(card => card.type === "resource");
		},
		"characters": function() {
			return this.cards.filter(card => card.type === "character");
		},
		"tactics": function() {
			return this.cards.filter(card => card.type === "tactic");
		},
		"items": function() {
			return this.cards.filter(card => card.type === "item");
		},
		"locations": function() {
			return this.cards.filter(card => card.type === "location");
		},
		"cards_without_faction": function() {
			return this.cards.filter(card => card.type !== "faction");
		}
	},
	
	"methods": {
		"card_search_result": card_search_result,
		"show_card": show_card,
		"hide_card": hide_card,
		"add_card": add_card,
		"remove_card": remove_card,
		"import_from_file": import_from_file,
		"reset": reset,
		"validate": validate,
		"save_to_database": save_to_database,
		"export_to_text": export_to_text,
		"select_starting_resources": select_starting_resources,
		"export_to_octgn": export_to_octgn,
		"print_proxies": print_proxies
	}
};

function show_card(e, card_id) {
	set_cursor_image(e.pageX, e.pageY, `${card_id}.jpg`);
}

function hide_card() {
	remove_cursor_image();
}

function init_data() {
	return {
		"id": null,
		"name": null,
		"username": null,
		"cards": [],
		"card_search": []
	};
}

let cursor_image;

function ready() {
	Statistics.init();
	Statistics.update(this.cards_without_faction);
	
	cursor_image = $("#cursor-image");
}

function set_cursor_image(x, y, card_image) {
	const OFFSET = 30;
	
	const image_x = (x + OFFSET + cursor_image.outerWidth()) <= window.innerWidth
		? x + OFFSET
		: x - OFFSET - cursor_image.outerWidth();

	const scroll_top = $(window).scrollTop();
	let image_y;

	if (y - cursor_image.outerHeight() / 2 < scroll_top) {
		image_y = scroll_top;
	} else if (y + cursor_image.outerHeight() / 2 <= scroll_top + window.innerHeight) {
		image_y = y - cursor_image.outerHeight() / 2;
	} else {
		image_y = scroll_top + window.innerHeight - cursor_image.outerHeight();
	}

	cursor_image.attr("src", "/img/cards/" + card_image);
	cursor_image.offset({
		"top":	image_y,
		"left": image_x
	});
}

function remove_cursor_image() {
	// Changing the src-attribute led to weird behavior with 'mousemove'
	// trigger not happening.
	cursor_image.offset({ "top": -9999, "left": -9999 });
}

function card_search_result(cards) {
	this.card_search = cards;
}

function add_card(card) {
	if (card.type === "faction") {
		this.faction_card = card;
	} else {
		this.cards.push(card);

		if (card.supertype !== "staple") {
			Statistics.update(this.cards_without_faction);
		}
	}
}

function remove_card(card) {
	this.cards.$remove(card);
	
	if (card.supertype !== "staple") {
		Statistics.update(this.cards_without_faction);
	}
	
	if (!this.cards.some(c => c.name === card.name)) {
		remove_cursor_image();
	}
}

function import_from_file(e) {
	const file = e.target.files[0];
	
	Importer.from_file(file).then(cards => {
		this.reset();
		this.cards = cards;
		Statistics.update(this.cards_without_faction);
		Notice.display(L("DECK.IMPORTED"));
	}).catch(err => Notice.display(L(err)));
}

function reset() {
	this.$data = init_data();
	Statistics.clear();
	Statistics.update(this.cards_without_faction);
}

function validate(faction, cards) {
	Notice.clear();

	const errors = Validator.validate(faction, cards);

	errors.forEach(error => {
		Notice.display({ "type": "error", "text": error });
	});

	if (errors.length === 0) {
		display(L("DECK.VALID"));
	}
}

function save_to_database() {
	let request;
	
	switch (this.save_type) {
		case "create":
			request = this.submit({
				"url": "/decks",
				"method": "POST",
				"data": Exporter.to_database(this.name, this.cards)
			}).done(() => Notice.display(L("DECK.SAVED")));
			break;
		
		case "update":
			request = this.submit({
				"url": `/decks/${this.id}`,
				"method": "PATCH",
				"data": Exporter.to_database(this.name, this.cards)
			}).done(() => Notice.display(L("DECK.UPDATED")));
			break;
		
		case "clone":
			request = this.submit({
				"url": "/decks",
				"method": "POST",
				"data": Exporter.to_database(`${this.name} (clone)`, this.cards)
			}).done(() => Notice.display(L("DECK.CLONED")));
			break;
	}
	
	request.done(() => {
		this.reset();
		this.$router.go("/decks/mine");
	});
}

function export_to_text() {
	const content = Exporter.to_text(this.faction_card, this.cards);
	send_file((this.name || "Deck") + ".txt", content);
}

function select_starting_resources() {
	const selectize = $("select[name='starting-resources']").selectize({
		"plugins": ["remove_button"],
		"maxItems": 2,
		"preload": "focus"
	})[0].selectize;

	selectize.clear();
	selectize.clearOptions();

	const staples = this.cards.filter(card => card.supertype === "staple");
	const options = staples.map(card => {
		return {
			"text": card.name,
			
			// Selectize doesn't handle multiple options with same
			// value, hence the Math.random(). Track this for updates:
			// https://github.com/selectize/selectize.js/issues/129
			"value": JSON.stringify({
				"id": card.id,
				"name": card.name,
				"rnd": Math.random()
			})
		};
	});

	selectize.load(callback => {
		callback(options);
	});

	$("#select-starting-resources-modal").modal();
}

function export_to_octgn() {
	const starting_resources = $("select[name='starting-resources']")[0].selectize.items.map(card => JSON.parse(card));
	const content = Exporter.to_octgn(this.faction_card, starting_resources, this.cards_without_faction);
	send_file((this.name || "Deck") + ".o8d", content);
}

function send_file(name, content) {
	save_as(new Blob(content, { "type": "text/plain; charset=utf-8" }), name);
}

function print_proxies() {
	const proxies = this.cards.map(card => card.id);

	if (this.name) {
		Local.set("name", this.name);
	} else {
		Local.remove("name");
	}
	
	Local.set("proxies", proxies);
	window.open("/deck-builder/proxies");
}
