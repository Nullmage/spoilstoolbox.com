import $ from "jquery";

export default {
	"ready": ready,
	"methods": {
		"open": open,
		"close": close
	}
};

function ready() {
	const modal = $(this.$els.modal);
	
	if (modal.length === 0) {
		return console.error("No modal element registered", this);
	}
	
	modal.on("hidden.bs.modal", () => {
		this.$emit("modal-closed");
	});
	
	this.modal = modal;
}

function open() {
	this.modal.modal("show");
}

function close() {
	this.modal.modal("hide");
}