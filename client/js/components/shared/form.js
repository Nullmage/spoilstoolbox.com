import $ from "jquery";

import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";

import SubmitButton from "./submit-button.js";
import FormError from "./form-error.js";

export default {
	"ready": ready,
	"components": {
		"submit-button": SubmitButton,
		"form-error": FormError
	},
	
	"data": function() {
		return {
			"errors": {}
		};
	},
	
	"methods": {
		"submit": submit,
		"serialize": serialize,
		"reset_form": reset_form,
		"reset_errors": reset_errors
	}
};

function ready() {
	const form = $(this.$els.form);
	
	if (form.length === 0) {
		return console.error("No form element found (forgot v-el:form?)", this);
	}
	
	if (!this.$refs.submit_button) {
		return console.error("No <submit-button> found for", form);
	}
	
	if (ENVS.test) {
		form.prop("novalidate", true);
	} else {
		init_validators();
	}
	
	this.form = form;
	
	function init_validators() {
		form.find("input[validate]").map((i, elem) => {
			const element = $(elem);
			const field = element.attr("validate");
			apply_validators(element, field);
		});
	}
	
	function apply_validators(element, field) {
		VALIDATORS[field].forEach(rule => {
			if (typeof(rule) === "string") {
				element.prop(rule, true);
			} else {
				const key = Object.keys(rule)[0];
				const value = rule[key];
				element.attr(key, value);
			}
		});
	}
}

const VALIDATORS = {
	"email": [
		"required"
	],
	
	"username": [
		"required",
		{ "pattern": ".{3,12}" },
		{ "title": L("ACCOUNT.USERNAME_INVALID_LENGTH") }
	],
	
	"password": [
		"required",
		{ "pattern": ".{8,}" },
		{ "title": L("ACCOUNT.PASSWORD_TOO_SHORT") }
	],
	
	"email_or_username": [
		"required"
	],
	
	"deck.name": [
		"required"
	]
};

function submit(options = {}) {
	const self = this;
	const form = this.form;
	form.data("state", "submit");
	this.reset_errors();
	
	return self.$refs.submit_button.ajax({
		"url": options.url || form.attr("action"),
		"method": options.method || form.attr("method"),
		"data": options.data || form.serialize()
	}).done(response => {
		self.$emit("form-success", response);
		form.data("state", "success");
	}).fail(xhr => {
		const response = xhr.responseJSON;
		
		if (typeof(response) === "string") {
			const message = L(response);

			if (message) {
				display(message);
			}
		} else if (typeof(response) === "object") {
			set_field_errors(response);
			form.find(".has-error").first().find("input").focus();
		}
		
		form.data("state", "fail");
	});
	
	function set_field_errors(errors) {
		errors = Array.isArray(errors) ? errors : [errors];
		errors.forEach(err => set_field_error(err.field, L(err.code)));
	}
	
	function set_field_error(field, message) {
		form.find(`input[name='${field}']`).parents(".form-group").addClass("has-error");
		const component = self.errors[field];
		
		if (component) {
			component.message = message;
		} else {
			console.error(`<form-error> element missing for field '${field}'`);
		}
	}
}

function serialize() {
	return this.form.serializeArray().reduce((o, v) => {
		const curr_val = o[v.name];

		if (Array.isArray(curr_val)) {
			o[v.name].push(v.value);
		} else if (curr_val) {
			o[v.name] = [curr_val, v.value];
		} else {
			o[v.name] = v.value;
		}

		return o;
	}, {});
}

function reset_form() {
	this.form.trigger("reset");
	this.reset_errors();
}

function reset_errors() {
	this.form.find(".has-error").removeClass("has-error");
	
	for (const field in this.errors) {
		this.errors[field].message = null;
	}
}
