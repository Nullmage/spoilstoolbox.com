import Modal from "js/components/shared/modal.js";

import Template from "./contribute.html";

export default {
	"template": Template,
	"mixins": [Modal]
};
