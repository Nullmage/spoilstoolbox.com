import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";
import Modal from "js/components/shared/modal.js";
import Form from "js/components/shared/form.js";

import Template from "./recover-account.html";

export default {
	"ready": ready,
	"template": Template,
	"mixins": [Modal, Form]
};

function ready() {
	this.$on("form-success", () => {
		display(L("ACCOUNT.RECOVERY_EMAIL_SENT"));
		this.close();
	});

	this.$on("modal-closed", () => this.reset_form());
}