import $ from "jquery";
import Vue from "vue";

import Modal from "js/components/shared/modal.js";

import Template from "./card.html";

Vue.filter("price", function(price) {
	return price === 0 ? "N/A" : price.toFixed(2) + " €";
});

Vue.filter("card-text", function(text) {
	if (!text) {
		return;
	}

	text = text.replace(/[A-Z]{2,99}/g, function(match) {
		return "<strong>" + match + "</strong>";
	});

	text = text.replace(/\n/g, "<br>");
	text = text.replace(/\[[OGDREV]\]/g, function(match) {
		switch (match) {
			case "[O]": return '<img src="/img/icons/arcanist.png">';
			case "[G]": return '<img src="/img/icons/banker.png">';
			case "[D]": return '<img src="/img/icons/rogue.png">';
			case "[R]": return '<img src="/img/icons/warlord.png">';
			case "[E]": return '<img src="/img/icons/gearsmith.png">';
			case "[V]": return '<img src="/img/icons/universal.png">';
		}
	});

	return text;
});

export default {
	"template": Template,
	"mixins": [Modal],
	"data": function() {
		return init_data();
	},
	
	"methods": {
		"set_card": set_card,
		"populate": populate
	}
};

function init_data() {
	return {
		"id": "",
		"name": "",
		"caption": "",
		"prices": [],
		"text": "",
		"formats": []
	};
}

function set_card(card) {
	this.$data = init_data();
	this.populate(card);
}

function populate(card) {
	this.id = card.id;
	this.name = card.name;

	const progress = setInterval(() => {
		this.i = this.i || 0;
		const dots = ".".repeat(++this.i % 4);
		this.caption = "Loading prices" + dots;
	}, 250);

	const request = $.get(`/cards/${card.name}/price`)
	.always(() => clearInterval(progress))
	.done(prices => {
		if (prices.length === 0) {
			this.caption = "No prices found";
		} else {
			this.caption = "Sets and Prices";
			this.prices = prices;
		}
	}).fail(xhr => {
		this.caption = "Failed loading prices :(";
	});

	$.get(`/cards/${card.name}/info`).done(info => {
		this.text = info.text;
		this.formats = info.format;
	});

	this.$on("modal-closed", () => {
		request.abort();
		clearInterval(progress);
	});
}
