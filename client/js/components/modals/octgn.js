import Modal from "js/components/shared/modal.js";

import Template from "./octgn.html";

export default {
	"template": Template,
	"mixins": [Modal]
};