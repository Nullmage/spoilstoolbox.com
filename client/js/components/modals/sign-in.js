import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";
import Modal from "js/components/shared/modal.js";
import Form from "js/components/shared/form.js";

import Template from "./sign-in.html";
import RecoverAccount from "./recover-account.js";

export default {
	"ready": ready,
	"template": Template,
	"mixins": [Modal, Form],
	"components": {
		"recover-account": RecoverAccount
	},

	"methods": {
		"open_recover_account": open_recover_account
	}
};

function ready() {
	this.$on("form-success", response => {
		this.$root.user.sign_in(response);
		display(L("ACCOUNT.SIGNED_IN"));
		this.close();
	});
	
	this.$on("modal-closed", () => this.reset_form());
}

function open_recover_account() {
	this.$refs.recover_account.open();
}
