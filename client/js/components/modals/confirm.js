import Events from "js/lib/events.js";
import Modal from "js/components/shared/modal.js";

import Template from "./confirm.html";

export default {
	"ready": ready,
	"template": Template,
	"mixins": [Modal],
	"data": function() {
		return {
			"title": null,
			"body": null,
			"btn_class": "btn-primary",
			"btn_text": null,
			"callback": null
		};
	},
	
	"methods": {
		"confirm": confirm,
		"cancel": cancel
	}
};

function ready() {
	Events.on("confirm", options => {
		this.$data = options;
		this.open();
	});
}

function confirm() {
	this.callback();
	this.close();
}

function cancel() {
	this.close();
}