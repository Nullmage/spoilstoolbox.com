import "tracekit";
import "bootstrap";
import "font-awesome.css";
import $ from "jquery";

import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";
import User from "js/lib/user.js";
import Router from "js/config/routes.js";
import Application from "js/components/application.js";

install_tracekit();
install_ajax_handlers();
start_application();

function install_tracekit() {
	TraceKit.report.subscribe(error => {
		$.post("/client-error", error);
	});
}

function install_ajax_handlers() {
	$(document).ajaxSend((e, xhr) => {
		if (User.authed) {
			xhr.setRequestHeader("X-CSRF-TOKEN", User.csrf_token);
		}
		
		// $(document).ajaxError(...) global handler will not be invoked
		// since a .fail() promise handler is attached to all the request.
		xhr.fail(ajax_error);
	});
	
	function ajax_error(xhr) {
		switch (xhr.status) {
			case 401:
				$("#sign-in-modal").modal();
				// fall through
			case 403:
				// fall through
			case 408:
				// fall through
			case 500:
				display(L(`HTTP.${xhr.status}`));
				break;
		}
	}
}

function start_application() {
	Router.start(Application, "#application", () => {
		NProgress.done();
		setTimeout(fade_in_page, 200);
	});
	
	function fade_in_page() {
		const container = $("#application");
		container.animate({ "opacity": 1 }, 200, () => {
			container.data("ready", true);
		});
	}
}
