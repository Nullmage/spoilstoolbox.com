import $ from "jquery";
import Vue from "vue";
import VueRouter from "vue-router";

import User from "js/lib/user.js";
import { Session } from "js/lib/storage.js";
import { L } from "js/config/messages.js";
import { display } from "js/lib/notice.js";

import News from "js/components/news.js";
import Cards from "js/components/cards.js";

import RecentDecks from "js/components/decks/recent.js";
import MyDecks from "js/components/decks/mine.js";
import DeckSearch from "js/components/decks/search.js";

import DeckBuilder from "js/components/deck-builder.js";
import ActivateAccount from "js/components/account/activate.js";
import SetPassword from "js/components/account/set-password.js";
import AccountSettings from "js/components/account/settings.js";

Vue.use(VueRouter);
const Router = new VueRouter();

Router.map({
	"/": {
		"component": Cards
	},
	
	"/news": {
		"component": News
	},
	
	"/cards": {
		"component": Cards
	},
	
	"/decks": {
		"component": {
			"template": "<div><router-view></router-view></div>"
		},
		
		"subRoutes": {
			"/": {
				"component": RecentDecks
			},
			
			"/recent": {
				"component": RecentDecks
			},
			
			"/mine": {
				"require_auth": true,
				"component": MyDecks
			},
			
			"/search": {
				"component": DeckSearch
			}
		}
	},
	
	"/deck-builder": {
		"component": {
			"template": "<div><router-view></router-view></div>"
		},
		
		"subRoutes": {
			"/": {
				"component": DeckBuilder
			},
			
			"/:id": {
				"component": DeckBuilder
			}
		}
	},
	
	"/account/settings": {
		"component": AccountSettings
	},
	
	"/account/activate/:token": {
		"component": ActivateAccount
	},
	
	"/account/set-password/:token": {
		"component": SetPassword
	}
});

Router.redirect({
	"*": "/"
});

Router.beforeEach(t => {
	Session.remove("local");
	
	if (t.to.require_auth && !User.authed) {
		display(L("HTTP.401"));
		$("#sign-in-modal").modal();
		t.abort();
	} else {
		if (t.from.path) {
			// Don't start/stop NProgress on intial page load
			NProgress.start();
		}
		
		t.next();
	}
});

Router.afterEach(t => {
	if (t.from.path) {
		NProgress.done();
	}
});

export default Router;
