export function L(code, ...args) {
	const message = MESSAGES[code];
	
	if (message) {
		if (typeof(message) === "string") {
			return sprintf(message, args).replace(/\n/g, "<br>");
		} else {
			const cloned = clone(message);
			cloned.text = sprintf(cloned.text, args).replace(/\n/g, "<br>");
			return cloned;
		}
	}
}

function sprintf(text, ...args) {
	return text.replace(/%s/g, () => args.shift());
}

function clone(o) {
	return JSON.parse(JSON.stringify(o));
}

const MESSAGES = {
	// card-search-form.js (component)
	"CARD_SEARCH.ERROR": {
		"type": "error",
		"text": "%s"
	},
	"CARD_SEARCH.EMPTY": {
		"type": "warning",
		"text": "Your search didn't match any cards."
	},
	"CARD_SEARCH.RESULT": {
		"type": "success",
		"text": "%s cards(s) found."
	},
	
	// deck.js (controller)
	"DECK.IMPORTED": {
		"type": "success",
		"text": "Your deck has been imported."
	},
	"DECK.VALID": {
		"type": "success",
		"text": "Your deck is valid."
	},
	"DECK.SAVED": {
		"type": "success",
		"text": "Your deck has been saved."
	},
	"DECK.UPDATED": {
		"type": "success",
		"text": "Your deck has been updated."
	},
	"DECK.CLONED": {
		"type": "success",
		"text": "The deck has been cloned."
	},
	"DECK.PUBLISHED": {
		"type": "success",
		"text": "Your deck has been published."
	},
	"DECK.UNPUBLISHED": {
		"type": "success",
		"text": "Your deck has been unpublished."
	},
	"DECK.TRASHED": {
		"type": "success",
		"text": "Your deck has been trashed."
	},
	"DECK.INVALID_FILE_FORMAT": {
		"type": "error",
		"title": "Invalid Format",
		"text": "The file appears to be in an invalid format (only .txt and .o8d are considered valid)."
	},
	"DECK.NOT_FOUND": {
		"type": "error",
		"title": "Not Found",
		"text": "The requested deck was not found."
	},
	
	// form errors
	"DECK.NAME_REQUIRED": "A deck must have a name.",
	
	// view-more.js (component)
	"VIEW_MORE.EMPTY": {
		"type": "warning",
		"text": "No more entries to fetch."
	},
	
	// account.js (controller)
	"FEEDBACK_SENT": {
		"type": "success",
		"text": "Thank you for your feedback!"
	},
	
	// Form errors
	"ACCOUNT.EMAIL_INVALID": "Please enter a valid email address.",
	"ACCOUNT.EMAIL_DISPOSABLE": "Disposable email addresses are not allowed.",
	"ACCOUNT.EMAIL_REGISTERED": "Email address is already registered.",
	"ACCOUNT.USERNAME_INVALID_LENGTH": "Username must be between 3 and 12 characters.",
	"ACCOUNT.USERNAME_ALNUM_ONLY": "Username can only consist of alphabetical characters and digits.",
	"ACCOUNT.USERNAME_REGISTERED": "Username is already registered.",
	"ACCOUNT.PASSWORD_TOO_SHORT": "Password must be at least 8 characters.",
	"ACCOUNT.BY_EMAIL_NOT_FOUND": "No accounts were found matching the provided email.",
	"ACCOUNT.BY_USERNAME_NOT_FOUND": "No accounts were found matching the provided username.",
	
	"ACCOUNT.CREATED": {
		"type": "success",
		"title": "Account Created",
		"hide": false,
		"text": `An email has been sent to you with an activation link. To be able to log in, you'll first need to activate your account by clicking on the link in the email.
		Please check your spam folder if you can't find the email.`
	},
	"ACCOUNT.ACTIVATED": {
		"type": "success",
		"title": "Account Activated",
		"text": "Your account has been activated."
	},
	"ACCOUNT.ACTIVATION_FAILED": {
		"type": "error",
		"title": "Account Activation Failed",
		"hide": false,
		"text": `Your account is already activated or your activation token is invalid.<br>
		1. If you haven't received the account activation email, check your spam folder.<br>
		2. Try signing in to your account with your credentials.<br>
		3. If you have forgotten your username or password, use the <a data-toggle="modal" data-target="#recover-account-modal">account recovery form</a>.<br>
		4. If you still have trouble accessing your account, <a data-toggle="modal" data-target="#contact-modal">send me a message</a>.`
	},
	"ACCOUNT.SIGNED_IN": {
		"type": "success",
		"text": "You are now signed in."
	},
	"ACCOUNT.SIGNED_OUT": {
		"type": "success",
		"text": "You are now signed out." 
	},
	"ACCOUNT.RECOVERY_EMAIL_SENT": {
		"type": "success",
		"title": "Email Sent",
		"hide": false,
		"text": "An email has been sent to you with further instructions on how to recover your account. Please check your spam folder if you can't find the email."
	},
	"ACCOUNT.INVALID_CREDENTIALS": {
		"type": "error",
		"title": "Invalid Credentials",
		"text": "The username or password you have entered is invalid."
	},
	"ACCOUNT.NOT_ACTIVATED": {
		"type": "error",
		"title": "Account Not Activated",
		"text": "Looks like your account has not been activated yet. In order to activate it, click on the link that was sent in the email when you registered. Please check your spam folder in case you can't find the email."
	},
	"ACCOUNT.RECOVERY_COOLDOWN": {
		"type": "error",
		"title": "Slow down...",
		"text": "An email has already been sent to you. A recovery attempt may be done once every 24 hours. Please check your spam folder if you can't find the email."
	},
	"ACCOUNT.INVALID_RECOVERY_TOKEN": {
		"type": "error",
		"title": "Set Password Failed",
		"hide": false,
		"text": `Your recovery token is either invalid or already used.<br>
		1. You may only set a new password once per recovery attempt. You may request <a data-toggle="modal" data-target="#recover-account-modal">a new recovery link</a> if you wish, but this may be done only once every 24h.<br>
		2. If you still have trouble accessing your account, <a data-toggle="modal" data-target="#contact-modal">send me a message</a>.`
	},
	"ACCOUNT.PASSWORD_SET": {
		"type": "success",
		"title": "Password Updated",
		"text": "Your password has been updated. You may now sign in with your new credentials."
	},
	"ACCOUNT.CURRENT_PASSWORD_MISMATCH": {
		"type": "error",
		"text": "The current password you've entered is incorrect."
	},
	"ACCOUNT.PASSWORD_CHANGED": {
		"type": "success",
		"text": "Your password has been changed."
	},
	
	// HTTP errors
	"HTTP.401": {
		"type": "error",
		"title": "Unauthorized",
		"text": "You need to be signed in to your account in order to access that page."
	},
	"HTTP.403": {
		"type": "error",
		"title": "Forbidden",
		"text": "You don't have permission for the requested operation."
	},
	"HTTP.408": {
		"type": "error",
		"title": "Time out",
		"text": "Your request to the server timed out. Try again at a later time."
	},
	"HTTP.500": {
		"type": "error",
		"title": "Internal Server Error",
		"text": "Looks like something went wrong on our end! These errors are tracked automatically, but if the problem persists feel free to <a data-toggle='modal' data-target='#contact-modal'>send me a message</a>."
	}
};
