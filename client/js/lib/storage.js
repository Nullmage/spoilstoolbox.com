class Storage {
	constructor(store) {
		this.store = store;
	}
	
	get(key) {
		const value = this.store.getItem(key);
		
		try {
			return JSON.parse(value);
		} catch (e) {
			return value;
		}
	}

	set(key, value) {
		this.store.setItem(`${key}-exist`, true);
		this.store.setItem(key, typeof(value) === "object" ? JSON.stringify(value) : value);
	}
	
	has(key) {
		return this.store.getItem(`${key}-exist`);
	}
	
	remove(key) {
		this.store.removeItem(`${key}-exist`);
		this.store.removeItem(key);
	}
	
	clear() {
		this.store.clear();
	}
	
	get local() {
		return this.get("local");
	}
	
	set local(value) {
		this.set("local", value);
	}
}

class Local extends Storage {
	constructor() {
		super(localStorage);
	}
}

class Session extends Storage {
	constructor() {
		super(sessionStorage);
	}
}

const LocalInstance = new Local();
const SessionInstance = new Session();

export {
	LocalInstance as Local,
	SessionInstance as Session
};