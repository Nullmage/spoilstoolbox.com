import { Session } from "./storage.js";

class User {
	constructor() {
		this.sync_data();
	}
	
	sync_data() {
		this.authed = Session.get("authed");
		this.username = Session.get("username");
		this.csrf_token = Session.get("csrf-token");
	}
	
	sign_in(data) {
		Session.set("authed", true);
		Session.set("username", data.username);
		Session.set("csrf-token", data.csrf_token);
		this.sync_data();
	}
	
	sign_out() {
		Session.clear();
		this.sync_data();
	}
}

export default new User;
