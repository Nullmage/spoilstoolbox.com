import $ from "jquery";
import moment from "moment";

export function cost(cost) {
	return `<strong>${cost !== undefined ? cost : ''}</strong>`;
}

export function threshold(value) {
	if (!value) {
		return "";
	}
	
	return value.replace(/[ogdrev]/g, c => {
		switch (c) {
			case "o": return '<img src="/img/icons/arcanist.png">';
			case "g": return '<img src="/img/icons/banker.png">';
			case "d": return '<img src="/img/icons/rogue.png">';
			case "r": return '<img src="/img/icons/warlord.png">';
			case "e": return '<img src="/img/icons/gearsmith.png">';
			case "v": return '<img src="/img/icons/universal.png">';
		}
	});
}

export function stack_cards(cards) {
	const stacks = {};
	
	cards.forEach(card => {
		stacks[card.name] = stacks[card.name] || { "copies": 0 };
		stacks[card.name].card = card;
		stacks[card.name].copies++;
	});
	
	return $.map(stacks, stack => {
		return stack;
	});
}

export function sort_stacks(stacks) {
	return stacks.sort((s1, s2) => {
		const cost_diff = s1.card.cost - s2.card.cost;

		if (cost_diff != 0) {
			return cost_diff;
		}

		const thresh1 = s1.card.threshold ? s1.card.threshold.length : 0;
		const thresh2 = s2.card.threshold ? s2.card.threshold.length : 0;
		const thresh_diff = thresh1 - thresh2;

		if (thresh_diff != 0) {
			return thresh_diff;
		}

		return s1.card.name.localeCompare(s2.card.name);
	});
}

export function time(value, format) {
	return moment.unix(value).format(format);
}

export function time_ago(value) {
	return moment.unix(value).fromNow();
}
