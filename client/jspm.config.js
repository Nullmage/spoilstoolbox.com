SystemJS.config({
  transpiler: "plugin-babel",
  babelOptions: {},
  meta: {
    "*.css": {
      "loader": "plugin-css"
    },
    "*.html": {
      "loader": "plugin-text"
    },
    "js/lib/notice.js": {
      "deps": [
        "pnotify-buttons"
      ]
    },
    "block-ui": {
      "deps": [
        "jquery"
      ],
      "format": "global"
    },
    "bootstrap": {
      "deps": [
        "bootstrap.css",
        "stylesheets/main.css",
        "jquery"
      ]
    },
    "jquery.lazyload": {
      "deps": [
        "jquery.lazyload.css",
        "jquery"
      ]
    },
    "highcharts": {
      "deps": [
        "jquery"
      ],
      "exports": "Highcharts"
    },
    "pnotify": {
      "deps": [
        "pnotify.css"
      ]
    },
    "pnotify-buttons": {
      "deps": [
        "pnotify-buttons.css",
        "pnotify"
      ]
    },
    "selectize": {
      "deps": [
        "selectize.css"
      ]
    },
    "tracekit": {
      "exports": "TraceKit"
    }
  },
  map: {
    "block-ui": "https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js",
    "bootstrap": "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js",
    "bootstrap.css": "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css",
    "bootstrap.theme.css": "https://maxcdn.bootstrapcdn.com/bootswatch/latest/flatly/bootstrap.min.css",
    "file-saver": "https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2014-11-29/FileSaver.min.js",
    "font-awesome.css": "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.1/css/font-awesome.css",
    "highcharts": "https://cdnjs.cloudflare.com/ajax/libs/highcharts/4.2.4/highcharts.js",
    "jquery": "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js",
    "jquery.lazyload": "https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyloadxt/1.1.0/jquery.lazyloadxt.min.js",
    "jquery.lazyload.css": "https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyloadxt/1.1.0/jquery.lazyloadxt.fadein.min.css",
    "moment": "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js",
    "pnotify": "https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.js",
    "pnotify-buttons": "https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.js",
    "pnotify-buttons.css": "https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.buttons.min.css",
    "pnotify.css": "https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.0.0/pnotify.min.css",
    "selectize": "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/js/standalone/selectize.min.js",
    "selectize.css": "https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.default.min.css",
    "tracekit": "js/vendor/tracekit.min.js",
    "vue": "https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.min.js",
    "vue-router": "https://cdnjs.cloudflare.com/ajax/libs/vue-router/0.7.13/vue-router.min.js"
  }
});

SystemJS.config({
  packageConfigPaths: [
    "github:*/*.json",
    "npm:@*/*.json",
    "npm:*.json"
  ],
  map: {
    "plugin-babel": "npm:systemjs-plugin-babel@0.0.12",
    "plugin-css": "github:systemjs/plugin-css@0.1.22",
    "plugin-text": "github:systemjs/plugin-text@0.0.8"
  },
  packages: {}
});
